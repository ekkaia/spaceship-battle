use crate::diesel::BoolExpressionMethods;
use crate::diesel::ExpressionMethods;
use diesel::OptionalExtension;
use diesel::{QueryDsl, RunQueryDsl};
use uuid::Uuid;

use crate::{schema::spaceship::engine, DBPooledConnection};

use super::models::Engine;
use super::models::NewEngine;

pub fn find_engine_list_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<Engine>, diesel::result::Error> {
    return engine::table
        .filter(engine::member_id.eq(member_id))
        .load::<Engine>(conn);
}

pub fn insert_engine(
    new_engine: NewEngine,
    conn: &DBPooledConnection,
) -> Result<Engine, diesel::result::Error> {
    return diesel::insert_into(engine::table)
        .values(&new_engine)
        .get_result(conn);
}

pub fn find_engine_by_member_and_engine_model(
    member_id: &Uuid,
    engine_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Engine, diesel::result::Error> {
    return engine::table
        .filter(
            engine::member_id
                .eq(member_id)
                .and(engine::engine_model_id.eq(engine_model_id)),
        )
        .first::<Engine>(conn);
}

pub fn find_one_engine_by_engine_id_and_member(
    engine_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Option<Engine>, diesel::result::Error> {
    return engine::table
        .filter(
            engine::engine_id
                .eq(engine_id)
                .and(engine::member_id.eq(member_id)),
        )
        .first::<Engine>(conn)
        .optional();
}

pub fn remove_spaceship_id_from_engine(
    engine_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    let result = diesel::update(
        engine::table.filter(
            engine::engine_id
                .eq(engine_id)
                .and(engine::member_id.eq(member_id))
                .and(engine::spaceship_id.is_not_null())
                .and(engine::module_slot_id.is_not_null()),
        ),
    )
    .set((
        engine::spaceship_id.eq(None::<Uuid>),
        engine::module_slot_id.eq(None::<Uuid>),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn update_by_engine_id_and_member(
    engine_id: &Uuid,
    module_slot_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    let result = diesel::update(
        engine::table.filter(
            engine::engine_id
                .eq(engine_id)
                .and(engine::member_id.eq(member_id))
                .and(
                    engine::spaceship_id
                        .is_null()
                        .or(engine::spaceship_id.eq(spaceship_id)),
                ),
        ),
    )
    .set((
        engine::spaceship_id.eq(spaceship_id),
        engine::module_slot_id.eq(module_slot_id),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn update_spaceship_id_from_engine(
    engine_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    let result = diesel::update(
        engine::table.filter(
            engine::engine_id
                .eq(engine_id)
                .and(engine::member_id.eq(member_id))
                .and(engine::spaceship_id.is_null()),
        ),
    )
    .set(engine::spaceship_id.eq(spaceship_id))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn remove_all_engines_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    return diesel::delete(engine::table.filter(engine::member_id.eq(member_id))).execute(conn);
}

pub fn delete_engines_by_model(
    engine_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    return diesel::delete(engine::table.filter(engine::engine_model_id.eq(engine_model_id)))
        .execute(conn);
}

pub fn remove_spaceship_id_from_engine_by_spaceship(
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    return diesel::update(engine::table.filter(engine::spaceship_id.eq(spaceship_id)))
        .set(engine::spaceship_id.eq(None::<Uuid>))
        .execute(conn);
}
