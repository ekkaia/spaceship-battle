use serde::{Serialize, Deserialize};
use uuid::Uuid;
use diesel::pg::types::sql_types::Jsonb;
use crate::shield_model::models::ShieldType;
use crate::schema::spaceship::shield;

#[derive(AsExpression, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
#[sql_type = "Jsonb"]
pub struct ShieldComponent {
	pub name: String,
	pub module_slot_id: Uuid,
	pub health: i32,
	pub max_power: i32,
	pub shield_id: Uuid,
	pub shield_model_id: Uuid,
	pub max_health: i32,
	pub reload_time: i32,
	pub shield_type: ShieldType,
	pub shield_type_protection: i32,
}

#[derive(Debug, Clone, Queryable, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct Shield {
	pub shield_id: Uuid,
	pub spaceship_id: Option<Uuid>,
	pub shield_model_id: Uuid,
	pub member_id: Uuid,
	pub module_slot_id: Option<Uuid>,
	pub health: i32,
	pub created_at: Option<chrono::NaiveDateTime>,
	pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Insertable, Debug, Deserialize, Serialize)]
#[table_name = "shield"]
pub struct NewShield {
	pub shield_model_id: Uuid,
	pub health: i32,
	pub member_id: Uuid,
}