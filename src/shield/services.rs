use diesel::{
    result::Error, BoolExpressionMethods, ExpressionMethods, OptionalExtension, QueryDsl,
    RunQueryDsl,
};
use uuid::Uuid;

use crate::{schema::spaceship::shield, DBPooledConnection};

use super::models::{NewShield, Shield};

pub fn find_shield_list_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<Shield>, Error> {
    return shield::table
        .filter(shield::member_id.eq(member_id))
        .load::<Shield>(conn);
}

pub fn insert_shield(new_shield: NewShield, conn: &DBPooledConnection) -> Result<Shield, Error> {
    return diesel::insert_into(shield::table)
        .values(&new_shield)
        .get_result(conn);
}

pub fn find_shield_by_member_and_shield_model(
    member_id: &Uuid,
    shield_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Shield, Error> {
    return shield::table
        .filter(
            shield::member_id
                .eq(member_id)
                .and(shield::shield_model_id.eq(shield_model_id)),
        )
        .first::<Shield>(conn);
}

pub fn update_by_shield_id_and_member(
    shield_id: &Uuid,
    module_slot_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::update(
        shield::table.filter(
            shield::shield_id
                .eq(shield_id)
                .and(shield::member_id.eq(member_id))
                .and(
                    shield::spaceship_id
                        .is_null()
                        .or(shield::spaceship_id.eq(spaceship_id)),
                ),
        ),
    )
    .set((
        shield::spaceship_id.eq(spaceship_id),
        shield::module_slot_id.eq(module_slot_id),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn remove_spaceship_id_from_shield(
    shield_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::update(
        shield::table.filter(
            shield::shield_id
                .eq(shield_id)
                .and(shield::member_id.eq(member_id))
                .and(shield::spaceship_id.is_not_null())
                .and(shield::module_slot_id.is_not_null()),
        ),
    )
    .set((
        shield::spaceship_id.eq(None::<Uuid>),
        shield::module_slot_id.eq(None::<Uuid>),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn find_one_shield_by_shield_id_and_member(
    shield_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Option<Shield>, Error> {
    return shield::table
        .filter(
            shield::shield_id
                .eq(shield_id)
                .and(shield::member_id.eq(member_id)),
        )
        .first::<Shield>(conn)
        .optional();
}

pub fn update_spaceship_id_from_shield(
    shield_id: &Uuid,
    spaceship_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::update(
        shield::table.filter(
            shield::shield_id
                .eq(shield_id)
                .and(shield::member_id.eq(member_id))
                .and(shield::spaceship_id.is_null()),
        ),
    )
    .set(shield::spaceship_id.eq(spaceship_id))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn remove_spaceship_id_from_shield_by_spaceship(
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    return diesel::update(shield::table.filter(shield::spaceship_id.eq(spaceship_id)))
        .set(shield::spaceship_id.eq(None::<Uuid>))
        .execute(conn);
}

pub fn remove_all_shields_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    return diesel::delete(shield::table.filter(shield::member_id.eq(member_id))).execute(conn);
}

pub fn delete_shields_by_model(
    shield_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    return diesel::delete(shield::table.filter(shield::shield_model_id.eq(shield_model_id)))
        .execute(conn);
}
