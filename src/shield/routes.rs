use actix_web::{HttpResponse, web::{block, ReqData, Data}, Error};
use serde_json::json;
use uuid::Uuid;

use crate::{DBPool, auth::models::Claims};

use super::services::find_shield_list_by_member;

pub async fn retrieve_shields_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let shields_found = block(move || find_shield_list_by_member(&member_id, &conn))
		.await
		.unwrap();

	match shields_found {
		Ok(shields) => Ok(HttpResponse::Ok().json(shields)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("No shield owned found: {}", err.to_string()),
			})));
		},
	}
}