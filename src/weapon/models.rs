use chrono::Utc;
use crate::weapon_model::models::{AmmoType, DamageType, WeaponType};
use serde::{Deserialize, Serialize};
use uuid::Uuid;
use crate::schema::spaceship::weapon;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct WeaponComponent {
	pub weapon_id: Uuid,
	pub weapon_model_id: Uuid,
	pub module_slot_id: Uuid,
	pub name: String,
	pub damage: i32,
	pub health: i32,
	pub max_health: i32,
	pub max_ammo: i32,
	pub ammo_type: AmmoType,
	pub max_power: i32,
	pub reload_time: i32,
	pub next_shot_time: chrono::DateTime<Utc>,
	pub damage_type: DamageType,
	pub weapon_type: WeaponType,
	pub ammo: i32,
	pub max_shooting_distance: i32,
	pub min_shooting_distance: i32,
	pub travel_time_by_distance: i32,
}

#[derive(Debug, Clone, Queryable, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct Weapon {
	pub weapon_id: Uuid,
	pub spaceship_id: Option<Uuid>,
	pub weapon_model_id: Uuid,
	pub module_slot_id: Option<Uuid>,
	pub member_id: Uuid,
	pub health: i32,
	pub ammo: i32,
	pub created_at: Option<chrono::NaiveDateTime>,
	pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Insertable, Debug, Deserialize, Serialize)]
#[table_name = "weapon"]
pub struct NewWeapon {
	pub weapon_model_id: uuid::Uuid,
	pub member_id: uuid::Uuid,
	pub health: i32,
	pub ammo: i32,
}