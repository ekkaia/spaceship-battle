use crate::{shield_model::models::NewShieldModel, auth::models::Claims, member::services::{find_one_member_by_id, update_member_credits}, shield::services::{insert_shield, find_shield_by_member_and_shield_model, delete_shields_by_model}};
use actix_web::{Result, HttpResponse, web::{Data, Json, block, ReqData, Path}, Error, http::header::ContentType};
use serde_json::json;
use crate::shield::models::NewShield;
use crate::DBPool;
use uuid::Uuid;
use crate::DBPooledConnection;

use super::services::{insert_shield_model, find_one_shield_model_by_name, find_all_shield_models, find_one_shield_model_by_id, find_shield_models_by_member, update_shield_model, delete_shield_model};

pub async fn create(shield_model: Json<NewShieldModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let shield_model_name: String = shield_model.name.clone();

	let is_shield_model_same_name_exists = block(move || find_one_shield_model_by_name(shield_model_name, &conn))
.await
.unwrap();

	match is_shield_model_same_name_exists {
		Ok(_) => {
			return Ok(HttpResponse::Conflict().json(json!({ "message": "Shield model with same name already exists" })));
		},
		Err(_) => {
			let shield_model_inserted = block(move || insert_shield_model(shield_model, &db.get().unwrap()))
.await
.unwrap();

			match shield_model_inserted {
				Ok(shield_model_inserted) => {
					return Ok(HttpResponse::Created().content_type(ContentType::json()).json(shield_model_inserted));
				},
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to insert shield model: {}", err.to_string())})));
				}
			}
		}
	}
}

pub async fn list(db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let shield_models = block(move || find_all_shield_models(&conn))
.await
.unwrap();

	match shield_models {
		Ok(shield_models) => {
			return Ok(HttpResponse::Ok().content_type(ContentType::json()).json(shield_models));
		},
		Err(_) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to get shield models" })));
		}
	}
}

pub async fn retrieve_shield_models_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let shield_models_found = block(move || find_shield_models_by_member(&member_id, &conn))
		.await
		.unwrap();

	match shield_models_found {
		Ok(shield_models) => Ok(HttpResponse::Ok().json(shield_models)),
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving shield model owned by member: {}", err.to_string()),
			})));
		},
	}
}

pub async fn buy(claims: ReqData<Claims>, shield_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();
	let is_shield_model_exists = block(move || find_one_shield_model_by_id(&shield_model_id.into_inner(), &conn))
		.await
		.unwrap();

	let shield_model = match is_shield_model_exists {
		Ok(shield_model) => shield_model,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Shield model not found" }))),
	};

	let member_id = Uuid::parse_str(&claims.sub.clone()).unwrap();
	let sec_conn: DBPooledConnection = db.get().unwrap();
	let is_member_exists = block(move || find_one_member_by_id(member_id, &sec_conn))
		.await
		.unwrap();

	let member = match is_member_exists {
		Ok(member) => member,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Member not found" }))),
	};

	if member.credits < shield_model.price {
		return Ok(HttpResponse::BadRequest().json(json!({ "message": "Not enough credits" })));
	}

	let fourth_conn: DBPooledConnection = db.get().unwrap();
	let is_shield_model_already_owned = block(move || find_shield_by_member_and_shield_model(
		&member.member_id,
		&shield_model.shield_model_id,
		&fourth_conn
	))
		.await
		.unwrap();

	if is_shield_model_already_owned.is_ok() {
		return Ok(HttpResponse::Conflict().json(json!({ "message": "Shield model already owned" })));
	}

	let new_shield = NewShield {
		health: shield_model.max_health,
		member_id: member.member_id,
		shield_model_id: shield_model.shield_model_id,
	};

	let fifth_conn: DBPooledConnection = db.get().unwrap();
	let insert_new_shield = block(move || insert_shield(
		new_shield,
		&fifth_conn
	))
		.await
		.unwrap();

	if insert_new_shield.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert new shield" })));
	}

	let third_conn: DBPooledConnection = db.get().unwrap();
	let payment: i32 = member.credits - shield_model.price;
	let _ = block(move || update_member_credits(member_id, payment, &third_conn))
		.await
		.unwrap();

	return Ok(HttpResponse::Ok().json(json!({ "message": "Successfully bought shield model" })));
}

pub async fn update(shield_model_id: Path<Uuid>, shield_model: Json<NewShieldModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let shield_model_updated = block(move || update_shield_model(shield_model_id.into_inner(), shield_model.into_inner(), &conn))
		.await
		.unwrap();

	match shield_model_updated {
		Ok(shield_model_updated) => {
			return Ok(HttpResponse::Ok().json(json!(shield_model_updated)));
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to update shield model: {}", err.to_string())})));
		}
	}
}

pub async fn delete(shield_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let shield_model_id = shield_model_id.into_inner();

	let shield_conn = db.get().unwrap();
	let _ = block(move || delete_shields_by_model(&shield_model_id.clone(), &shield_conn))
		.await
		.unwrap();

	let shield_model_conn = db.get().unwrap();
	let shield_model_deleted = block(move || delete_shield_model(&shield_model_id.clone(), &shield_model_conn))
		.await
		.unwrap();

	match shield_model_deleted {
		Ok(_) => {
			return Ok(HttpResponse::NoContent().finish());
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to delete shield model: {}", err.to_string())})));
		}
	}
}