use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct Metadata {
    pub limit: i64,
    pub offset: i64,
    pub count: i64,
    pub search: Option<String>,
}