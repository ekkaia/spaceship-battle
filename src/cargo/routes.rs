use actix_web::{HttpResponse, web::{block, ReqData, Data}, Error};
use serde_json::json;
use uuid::Uuid;

use crate::{DBPool, auth::models::Claims};

use super::services::find_cargo_list_by_member;

pub async fn retrieve_cargos_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let cargos_found = block(move || find_cargo_list_by_member(&member_id, &conn))
		.await
		.unwrap();

	match cargos_found {
		Ok(cargos) => Ok(HttpResponse::Ok().json(cargos)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("No cargo owned found: {}", err.to_string()),
			})));
		},
	}	
}