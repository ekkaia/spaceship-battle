use crate::diesel::{BoolExpressionMethods, ExpressionMethods, QueryDsl, RunQueryDsl};
use crate::{schema::spaceship::spaceship, DBPooledConnection};
use diesel::OptionalExtension;
use uuid::Uuid;

use super::models::{NewSpaceship, Spaceship};

pub fn find_spaceship_by_member_and_spaceship_model(
	member_id: &Uuid,
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Spaceship, diesel::result::Error> {
	return spaceship::table
		.filter(
			spaceship::member_id
				.eq(member_id)
				.and(spaceship::spaceship_model_id.eq(spaceship_model_id)),
		)
		.first::<Spaceship>(conn);
}

pub fn find_spaceship_by_model(
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Vec<Spaceship>, diesel::result::Error> {
	return spaceship::table
		.filter(spaceship::spaceship_model_id.eq(spaceship_model_id))
		.load::<Spaceship>(conn);
}

pub fn insert_spaceship(
	new_spaceship: NewSpaceship,
	conn: &DBPooledConnection,
) -> Result<Spaceship, diesel::result::Error> {
	return diesel::insert_into(spaceship::table)
		.values(&new_spaceship)
		.get_result(conn);
}

pub fn find_spaceship_list_by_member(
	member_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Vec<Spaceship>, diesel::result::Error> {
	return spaceship::table
		.filter(spaceship::member_id.eq(member_id))
		.load::<Spaceship>(conn);
}

pub fn update_main_configuration(
	spaceship_id: &Uuid,
	is_main: bool,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	let result = diesel::update(spaceship::table.find(spaceship_id))
		.set(spaceship::is_main_ship.eq(is_main))
		.execute(conn);

	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}

pub fn update_name_by_id_and_member_id(
	spaceship_id: &Uuid,
	member_id: &Uuid,
	name: &String,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	let result = diesel::update(
		spaceship::table.filter(
			spaceship::spaceship_id
				.eq(spaceship_id)
				.and(spaceship::member_id.eq(member_id)),
		),
	)
	.set(spaceship::name.eq(name))
	.execute(conn);
	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}

pub fn find_spaceship_by_id_and_member_id(
	spaceship_id: &Uuid,
	member_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Option<Spaceship>, diesel::result::Error> {
	return spaceship::table
		.filter(
			spaceship::spaceship_id
				.eq(spaceship_id)
				.and(spaceship::member_id.eq(member_id)),
		)
		.first::<Spaceship>(conn)
		.optional();
}

pub fn update_name_by_spaceship_and_member(
	spaceship_id: &Uuid,
	member_id: &Uuid,
	name: &String,
	&is_main_ship: &bool,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	let result = diesel::update(
		spaceship::table.filter(
			spaceship::spaceship_id
				.eq(spaceship_id)
				.and(spaceship::member_id.eq(member_id)),
		),
	)
	.set((
		spaceship::name.eq(name),
		spaceship::is_main_ship.eq(is_main_ship)
	))
	.execute(conn);
	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}

pub fn remove_name_from_spaceship(
	spaceship_id: &Uuid,
	member_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	let result = diesel::update(
		spaceship::table.filter(
			spaceship::spaceship_id
				.eq(spaceship_id)
				.and(spaceship::member_id.eq(member_id))
				.and(spaceship::name.is_not_null()),
		),
	)
	.set(spaceship::name.eq(None::<String>))
	.execute(conn);
	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}

pub fn remove_all_spaceships_by_member(
	member_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	return diesel::delete(spaceship::table.filter(spaceship::member_id.eq(member_id))).execute(conn);
}

pub fn delete_spaceships_by_model(
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	return diesel::delete(spaceship::table.filter(spaceship::spaceship_model_id.eq(spaceship_model_id)))
		.execute(conn);
}