use crate::schema::spaceship::member;
use uuid::Uuid;
use serde::{Serialize, Deserialize};

#[derive(Debug, Queryable, Insertable, Deserialize, Serialize)]
#[table_name = "member"]
pub struct Member {
	pub member_id: Uuid,
	pub name: String,
	pub email: String,
	pub credits: i32,
	pub password: String,
	pub is_admin: bool,
	pub created_at: Option<chrono::NaiveDateTime>,
	pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Debug, Deserialize, Serialize, Queryable)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct ExposedMember {
	pub member_id: Uuid,
	pub name: String,
	pub email: String,
	pub is_admin: bool,
	pub credits: i32,
}

#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "member"]
pub struct NewMember {
	pub name: String,
	pub email: String,
	pub is_admin: bool,
	pub credits: i32,
	pub password: String,
}

#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "member"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct UpdateMember {
	pub name: String,
	pub email: String,
	pub credits: i32,
	pub is_admin: bool,
}

#[derive(Debug,Deserialize, Serialize, Clone)]
pub struct RetrieveMembersQueryParams {
	pub limit: i64,
	pub offset: i64,
	pub search: Option<String>,
}