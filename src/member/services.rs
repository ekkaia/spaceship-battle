use crate::{
	auth::models::Register,
	cargo::services::remove_all_cargos_by_member,
	diesel::ExpressionMethods,
	engine::services::remove_all_engines_by_member,
	member::models::{ExposedMember, UpdateMember, NewMember},
	schema::spaceship::member::{credits, email, is_admin, member_id, name},
	shield::services::remove_all_shields_by_member,
	spaceship::services::remove_all_spaceships_by_member,
	weapon::services::remove_all_weapons_by_member,
	DBPool,
};
use actix_web::{
	web::{block, Data},
	HttpResponse,
};
use diesel::{result::Error, QueryDsl, RunQueryDsl, TextExpressionMethods, BoolExpressionMethods};
use serde_json::json;
use uuid::Uuid;

use crate::{schema::spaceship::member, DBPooledConnection};

use super::models::Member;

pub fn find_one_member_by_id(
	requested_member_id: Uuid,
	conn: &DBPooledConnection,
) -> Result<ExposedMember, Error> {
	return member::table
		.select((member_id, name, email, is_admin, credits))
		.filter(member::member_id.eq(requested_member_id))
		.first::<ExposedMember>(conn);
}

pub fn find_one_member_by_email(
	requested_email: String,
	conn: &DBPooledConnection,
) -> Result<Member, Error> {
	return member::table
		.filter(member::email.eq(requested_email))
		.first::<Member>(conn);
}

pub fn find_one_member_by_name(
	requested_name: String,
	conn: &DBPooledConnection,
) -> Result<ExposedMember, Error> {
	return member::table
		.select((member_id, name, email, is_admin, credits))
		.filter(member::name.eq(requested_name))
		.first::<ExposedMember>(conn);
}

pub fn update_member(
	requested_member_id: Uuid,
	member: UpdateMember,
	conn: &DBPooledConnection,
) -> Result<Member, Error> {
	return diesel::update(member::table.find(requested_member_id))
		.set(member)
		.get_result(conn);
}

pub fn update_member_credits(
	requested_member_id: Uuid,
	new_credits_amount: i32,
	conn: &DBPooledConnection,
) -> Result<Member, Error> {
	return diesel::update(member::table.find(requested_member_id))
		.set(credits.eq(new_credits_amount))
		.get_result(conn);
}

pub fn insert_member(member: Register, conn: &DBPooledConnection) -> Result<ExposedMember, Error> {
	let new_member = NewMember {
		name: member.name,
		email: member.email,
		is_admin: false,
		credits: 50000,
		password: member.password,
	};

	type InsertResult = (Uuid, String, String, bool, i32);
	let res: Result<InsertResult, Error> = diesel::insert_into(member::table)
		.values(new_member)
		.returning((member_id, name, email, is_admin, credits))
		.get_result(conn);

	match res {
		Ok(inserted_member) => Ok(ExposedMember {
			member_id: inserted_member.0,
			name: inserted_member.1,
			email: inserted_member.2,
			is_admin: inserted_member.3,
			credits: inserted_member.4,
		}),
		Err(err) => Err(err),
	}
}

pub fn remove_member_by_id(
	requested_member_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, Error> {
	let result = diesel::delete(member::table.find(requested_member_id)).execute(conn);
	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}

pub async fn remove_totally_member(
	requested_member_id: Uuid,
	db: Data<DBPool>,
) -> Result<HttpResponse, actix_web::Error> {
	let shield_conn = db.get().unwrap();
	let delete_member_shields =
		block(move || remove_all_shields_by_member(&requested_member_id.clone(), &shield_conn))
			.await
			.unwrap();

	if delete_member_shields.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({ "message": "Error while deleting member shields" })));
	}

	let cargo_conn = db.get().unwrap();
	let delete_member_cargos =
		block(move || remove_all_cargos_by_member(&requested_member_id.clone(), &cargo_conn))
			.await
			.unwrap();

	if delete_member_cargos.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({ "message": "Error while deleting member cargos" })));
	}

	let weapon_conn = db.get().unwrap();
	let delete_member_weapons =
		block(move || remove_all_weapons_by_member(&requested_member_id.clone(), &weapon_conn))
			.await
			.unwrap();

	if delete_member_weapons.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({ "message": "Error while deleting member weapons" })));
	}

	let engine_conn = db.get().unwrap();
	let delete_member_engines =
		block(move || remove_all_engines_by_member(&requested_member_id.clone(), &engine_conn))
			.await
			.unwrap();

	if delete_member_engines.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({ "message": "Error while deleting member engines" })));
	}

	let spaceship_conn = db.get().unwrap();
	let delete_member_spaceships = block(move || {
		remove_all_spaceships_by_member(&requested_member_id.clone(), &spaceship_conn)
	})
	.await
	.unwrap();

	if delete_member_spaceships.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({ "message": "Error while deleting member spaceships" })));
	}

	let member_conn = db.get().unwrap();
	let delete_member =
		block(move || remove_member_by_id(&requested_member_id.clone(), &member_conn))
			.await
			.unwrap();

	if delete_member.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({ "message": "Error while deleting member" })));
	}

	return Ok(HttpResponse::NoContent().finish());
}

pub fn find_all_members(search: Option<String>, limit: i64, offset: i64, conn: &DBPooledConnection) -> Result<Vec<ExposedMember>, Error> {
	let mut query = member::table
		.select((member_id, name, email, is_admin, credits))
		.into_boxed();

	if let Some(search) = search {
		query = query.filter(name.like(format!("%{}%", search)).or(email.like(format!("%{}%", search))));
	}

	query = query.limit(limit).offset(offset);

	return query.load::<ExposedMember>(conn);
}

pub fn count_all_members(
	search: Option<String>,
	conn: &DBPooledConnection,
) -> Result<i64, Error> {
	let mut query = member::table.into_boxed();

	if let Some(search) = search {
		query = query.filter(name.like(format!("%{}%", search)).or(email.like(format!("%{}%", search))));
	}

	return query.count().get_result(conn);
}