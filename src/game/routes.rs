use std::time::Instant;

use actix::Addr;
use actix_web::{HttpRequest, web::{Data, Payload, block, Query}, HttpResponse, Error};
use actix_web_actors::ws;
use serde::Deserialize;
use serde_json::json;

use crate::{DBPool, auth::{services::token_decoding, models::TokenType}, configuration::services::find_main_configuration_by_member};
use crate::member::services::find_one_member_by_id;
use uuid::Uuid;

use super::models::{player::Player, game_server::GameServer};

#[derive(Deserialize)]
pub struct TokenQuery {
    pub token: String,
}

pub async fn room(
	req: HttpRequest,
	stream: Payload,
	game_server: Data<Addr<GameServer>>,
	query_params: Query<TokenQuery>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let result = token_decoding(query_params.token.clone());
	let claims = match result {
		Ok(claims) => {
			if claims.access_type != TokenType::WebSocketToken {
				return Ok(HttpResponse::Unauthorized().json(json!({ "message": "Wrong token" })));
			}
			claims
		},
		Err(e) => return Err(e),
	};

	let member_id = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let member_found = block(move || find_one_member_by_id(member_id, &conn))
		.await
		.unwrap();

	let member = match member_found {
		Ok(member) => member,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Unknow member id" }))),
	};

	let spaceship_found = block(move || find_main_configuration_by_member(&member_id, &db.get().unwrap()))
		.await
		.unwrap();

	let opt_spaceship = match spaceship_found {
		Ok(spaceship) => spaceship,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Error while retrieving main configuration" }))),
	};

	let spaceship = match opt_spaceship {
		Some(spaceship) => spaceship,
		None => return Ok(HttpResponse::NotFound().json(json!({ "message": "No main configuration found" }))),
	};

	let player = Player {
		member_id: member.member_id,
		name: member.name.clone(),
		spaceship,
		hb: Instant::now(),
		game_server: game_server.get_ref().clone(),
		room_id: None,
	};

	return ws::start(player, &req, stream);
}
