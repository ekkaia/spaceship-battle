use std::time::Instant;

use actix::{StreamHandler, ActorContext, AsyncContext};
use actix_http::ws::{ProtocolError, Message};
use serde::Deserialize;
use uuid::Uuid;

use crate::game::{models::{player::Player, actor_messages::AttackProcessingResult}, services::events::chat_event::PlayerMessage};

use super::events::attack_event::Attack;

#[derive(Deserialize)]
#[serde(untagged)]
pub enum StreamMessage {
	Attack(AttackStream)
}

#[derive(Debug, Deserialize)]
pub struct AttackStream {
	#[serde(rename(deserialize = "weaponId"))]
	pub weapon_id: Uuid,
}

impl StreamHandler<Result<Message, ProtocolError>> for Player {
	fn handle(&mut self, msg: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
		match msg {
			Ok(Message::Ping(msg)) => {
				self.hb = Instant::now();
				ctx.pong(&msg);
			}
			Ok(Message::Pong(_)) => {
				self.hb = Instant::now();
			}
			Ok(Message::Text(text)) => {
				let stream_message: Result<StreamMessage, serde_json::Error> = serde_json::from_str(&text);

				match stream_message {
					Ok(StreamMessage::Attack(attack)) => {
						let attack_processed = self.process_attack(attack.weapon_id);

						if let Some(attack_processed) = attack_processed {
							ctx.text(serde_json::to_string(&AttackProcessingResult::new(
								attack_processed.ammo,
								attack_processed.next_shot_time,
								attack.weapon_id,
							)).unwrap());

							let send_attack = Attack {
								player_addr: ctx.address(),
								room_id: self.room_id.unwrap(),
								damage: attack_processed.damage,
							};
							self.game_server.do_send(send_attack);
						}
					}
					Err(_) => {
						println!("Error parsing message: {}", text);
						let message_player = PlayerMessage {
							player_addr: ctx.address(),
							room_id: self.room_id.unwrap(),
							message: text.to_string(),
						};
						self.game_server.do_send(message_player);
					}
				}
			}
			Ok(Message::Binary(bin)) => {
				println!("Binary: {:?}", bin);
				ctx.binary(bin);
			}
			Ok(Message::Close(_)) => {
				ctx.stop();
			}
			Ok(Message::Nop) => (),
			Err(err) => {
				println!("Protocol error: {:?}", err);
				ctx.stop();
			}
			_ => (),
		}
	}

	fn started(&mut self, _ctx: &mut Self::Context) {}

	fn finished(&mut self, ctx: &mut Self::Context) {
		ctx.stop()
	}
}