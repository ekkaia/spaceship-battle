use actix::{Actor, Context};

use crate::game::models::game_server::GameServer;

impl Actor for GameServer {
	type Context = Context<Self>;
}