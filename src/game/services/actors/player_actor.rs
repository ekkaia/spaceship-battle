use actix::{Actor, fut, AsyncContext, WrapFuture, ActorFutureExt, ContextFutureSpawner};
use actix_web_actors::ws::WebsocketContext;

use crate::game::{services::events::{disconnection_event::Disconnect, connection_event::Connect, data_update_event::OpponentData}, models::{player::Player, actor_messages::{BattleMessageType, JoinRoomMessage}}};

impl Actor for Player {
	type Context = WebsocketContext<Self>;

	fn started(&mut self, ctx: &mut Self::Context) {
		self.hb(ctx);

		self.game_server.send(Connect {
			addr: ctx.address(),
			player_name: self.name.clone(),
			member_id: self.member_id.clone(),
			spaceship: self.spaceship.clone(),
		})
		.into_actor(self)
		.then(|conn_data, act, ctx| {
			match conn_data {
				Ok(connection_data) => {
					act.room_id = Some(connection_data.room_id);
					println!("Player {} joined room: {}", act.name, connection_data.room_id);
					match connection_data.opponent_player_addr {
						Some(opponent_player_addr) => {
							opponent_player_addr.send(OpponentData {
								name: act.name.clone(),
								spaceship: act.spaceship.clone(),
								message_type: BattleMessageType::OpponentData,
							}).into_actor(act)
							.then(|opponent_data, _act, ctx| {
								ctx.text(serde_json::to_string(&opponent_data.unwrap()).unwrap());
								fut::ready(())
							})
							.wait(ctx)
						},
						None => (),
					}
					let message = JoinRoomMessage::new(connection_data.room_state, connection_data.room_id);
					ctx.text(serde_json::to_string(&message).unwrap());
				},
				Err(err) => {
					println!("Player join error: {}", err);
				}
			}
			fut::ready(())
		})
		.wait(ctx);
	}

	fn stopped(&mut self, _ctx: &mut Self::Context) {
		println!("Websocket stopped");
	}

	fn stopping(&mut self, ctx: &mut Self::Context) -> actix::Running {
		println!("Websocket stopping");
		self.game_server.do_send(Disconnect {
			player_addr: ctx.address(),
			room_id: self.room_id.unwrap(),
		});
		return actix::Running::Stop;
	}
}