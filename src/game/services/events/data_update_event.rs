use actix::{Message, MessageResponse, Handler};
use actix_web_actors::ws::WebsocketContext;
use serde::Serialize;

use crate::{game::models::{actor_messages::{BattleMessageType, NewPlayerJoinedRoomMessage, StatusUpdate}, room::RoomState, player::Player}, configuration::models::Configuration};

#[derive(Message, MessageResponse, Serialize)]
#[rtype(result = "OpponentData")]
#[serde(rename_all(serialize = "camelCase"))]
pub struct OpponentData {
	pub name: String,
	pub spaceship: Configuration,
	pub message_type: BattleMessageType,
}

impl Handler<OpponentData> for Player {
	type Result = OpponentData;

	fn handle(&mut self, msg: OpponentData, ctx: &mut WebsocketContext<Self>) -> Self::Result {
		ctx.text(serde_json::to_string(&NewPlayerJoinedRoomMessage::new(
			RoomState::InGame, 
			msg.name, 
			msg.spaceship
		)).unwrap());
		return OpponentData {
			name: self.name.clone(),
			spaceship: self.spaceship.clone(),
			message_type: BattleMessageType::OpponentData,
		};
	}
}

impl Handler<StatusUpdate> for Player {
	type Result = ();

	fn handle(&mut self, opponent_status: StatusUpdate, ctx: &mut WebsocketContext<Self>) -> Self::Result {
		ctx.text(serde_json::to_string(&opponent_status).unwrap());
	}
}