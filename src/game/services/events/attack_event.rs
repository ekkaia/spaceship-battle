use actix::{Message, Addr, Handler, Context};
use actix_web_actors::ws::WebsocketContext;
use chrono::{Utc, Duration, DateTime};
use uuid::Uuid;

use crate::game::models::{player::Player, game_server::GameServer, actor_messages::{StatusUpdate, BattleMessageType}, room::RoomState};

#[derive(Debug, Message, Clone)]
#[rtype(result = "()")]
pub struct Attack {
	pub player_addr: Addr<Player>,
	pub room_id: Uuid,
	pub damage: i32,
}

impl Handler<Attack> for GameServer {
	type Result = ();

	fn handle(&mut self, attack: Attack, _ctx: &mut Context<Self>) {
		let room = self.rooms.iter().find(|r| r.room_id == attack.room_id);
		if let Some(room) = room {
			room.players.iter().for_each(|player| {
				if *player != attack.player_addr {
					player.do_send(attack.clone());
				}
			});
		}
	}
}

impl Handler<Attack> for Player {
	type Result = ();

	fn handle(&mut self, attack: Attack, ctx: &mut WebsocketContext<Self>) -> Self::Result {
		if self.spaceship.health > 0 {
			if self.spaceship.health - attack.damage > 0 {
				self.spaceship.health -= attack.damage;
			} else {
				self.spaceship.health = 0;
				println!("Player {} has been killed", self.name);
			}
			println!("{} got hit. Health: {}", self.name, self.spaceship.health);
		}
		ctx.text(serde_json::to_string(&StatusUpdate::new(
			self.spaceship.health, 
			BattleMessageType::SpaceshipStatusUpdate,
			false,
			if self.spaceship.health == 0 { RoomState::End } else { RoomState::InGame }
		)).unwrap());
		attack.player_addr.do_send(StatusUpdate::new(
			self.spaceship.health, 
			BattleMessageType::OpponentStatusUpdate,
			if self.spaceship.health == 0 { true } else { false },
			if self.spaceship.health == 0 { RoomState::End } else { RoomState::InGame }
		));
	}
}

pub struct AttackProcessed {
	pub damage: i32,
	pub ammo: i32,
	pub next_shot_time: DateTime<Utc>,
}

impl Player {
	pub fn process_attack(&mut self, weapon_id: Uuid) -> Option<AttackProcessed> {
		let weapon = self.spaceship.weapons.list.iter_mut().find(|weapon| weapon.weapon_id == weapon_id);

		match weapon {
			Some(weapon) => {
				if weapon.ammo > 0 && weapon.next_shot_time <= Utc::now() {
					weapon.ammo -= 1;
					weapon.next_shot_time = Utc::now() + Duration::milliseconds(weapon.reload_time as i64);
					println!("{} attacked with {}. {} ammos remaining", self.name, weapon.name, weapon.ammo);
					return Some(AttackProcessed {
						damage: weapon.damage,
						ammo: weapon.ammo,
						next_shot_time: weapon.next_shot_time,
					});
				}
				println!("Weapon is empty or reloading");
				return None;
			},
			None => {
				println!("Weapon not found");
				return None;
			}
		}
	}
}