use actix::{Message, Addr, Handler, Context};
use actix_web_actors::ws::WebsocketContext;
use uuid::Uuid;

use crate::game::models::{player::Player, game_server::GameServer};

#[derive(Debug, Clone, Message)]
#[rtype(result = "()")]
pub struct PlayerMessage {
	pub player_addr: Addr<Player>,
	pub room_id: Uuid,
	pub message: String,
}

impl Handler<PlayerMessage> for GameServer {
	type Result = ();

	fn handle(&mut self, player_message: PlayerMessage, _ctx: &mut Context<Self>) {
		let room = self
			.rooms
			.iter()
			.find(|room| room.room_id == player_message.room_id);

		if let Some(room) = room {
			room.players.iter().for_each(|player| {
				if *player != player_message.player_addr {
					player.do_send(player_message.clone());
				}
			});
		}
	}
}

impl Handler<PlayerMessage> for Player {
	type Result = ();

	fn handle(&mut self, msg: PlayerMessage, ctx: &mut WebsocketContext<Self>) {
		ctx.text(msg.message);
	}
}