use actix::{Message, Handler, Addr, Context};
use actix_web_actors::ws::WebsocketContext;
use uuid::Uuid;

use crate::game::models::{player::Player, game_server::GameServer, actor_messages::LeaveRoomMessage, room::RoomState};

#[derive(Debug, Message)]
#[rtype(result = "()")]
pub struct Disconnect {
	pub player_addr: Addr<Player>,
	pub room_id: Uuid,
}

impl Handler<Disconnect> for GameServer {
	type Result = ();

	fn handle(&mut self, disconnect: Disconnect, _ctx: &mut Context<Self>) {
		self.rooms
		.iter()
		.position(|r| r.room_id == disconnect.room_id)
		.map(|i| {
			self.rooms[i].players.retain(|player| *player != disconnect.player_addr);
			if self.rooms[i].players.len() == 0 {
				self.rooms.remove(i);
			} else {
				self.rooms[i].players[0].do_send(LeaveRoomMessage::new(RoomState::End));
			}
		});
	}
}

impl Handler<LeaveRoomMessage> for Player {
	type Result = ();

	fn handle(&mut self, msg: LeaveRoomMessage, ctx: &mut WebsocketContext<Self>) -> Self::Result {
		ctx.text(serde_json::to_string(&msg).unwrap());
	}
}