SELECT
    "weapon_model".weapon_model_id,
	"weapon_model".name,
	"weapon_model".weapon_type,
	"weapon_model".max_power,
	"weapon_model".max_ammo,
	"weapon_model".max_health,
	"weapon_model".reload_time,
	"weapon_model".damage,
	"weapon_model".damage_type,
	"weapon_model".ammo_type,
	"weapon_model".travel_time_by_distance,
	"weapon_model".min_shooting_distance,
	"weapon_model".max_shooting_distance,
	"weapon_model".size,
	"weapon_model".price
FROM "spaceship".weapon
JOIN "spaceship".weapon_model ON "weapon".weapon_model_id = "weapon_model".weapon_model_id
WHERE "weapon".member_id = $1