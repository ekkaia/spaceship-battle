use crate::schema::spaceship::weapon_model;
use diesel_derive_enum::DbEnum;
use serde::{Serialize, Deserialize};
use diesel::{Insertable, Queryable};
use uuid::Uuid;

#[derive(Debug, Serialize, Clone, DbEnum, Deserialize)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum WeaponType {
	Missile,
	Gatling,
	Railgun,
}

#[derive(Debug, Serialize, Clone, DbEnum, Deserialize)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum DamageType {
	Kinetic,
	Electric,
	Explosion,
}

#[derive(Debug, Serialize, Clone, DbEnum, Deserialize)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum AmmoType {
	HeavyAmmo,
	ElectricBulk,
}

#[derive(Debug, Queryable, QueryableByName, Deserialize, Serialize)]
#[table_name = "weapon_model"]
#[serde(rename_all = "camelCase")]
pub struct WeaponModel {
	pub weapon_model_id: Uuid,
	pub name: String,
	pub weapon_type: WeaponType,
	pub max_power: i32,
	pub max_ammo: i32,
	pub max_health: i32,
	pub reload_time: i32,
	pub damage: i32,
	pub damage_type: DamageType,
	pub ammo_type: AmmoType,
	pub travel_time_by_distance: i32,
	pub min_shooting_distance: i32,
	pub max_shooting_distance: i32,
	pub size: i32,
	pub price: i32,
}

#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "weapon_model"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewWeaponModel {
	pub name: String,
	pub weapon_type: WeaponType,
	pub max_power: i32,
	pub max_ammo: i32,
	pub max_health: i32,
	pub reload_time: i32,
	pub damage: i32,
	pub damage_type: DamageType,
	pub ammo_type: AmmoType,
	pub travel_time_by_distance: i32,
	pub min_shooting_distance: i32,
	pub max_shooting_distance: i32,
	pub size: i32,
	pub price: i32,
}