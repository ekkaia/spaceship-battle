use crate::{weapon_model::models::NewWeaponModel, member::services::{update_member_credits, find_one_member_by_id}, auth::models::Claims, weapon::{models::NewWeapon, services::{find_weapon_by_member_and_weapon_model, delete_weapons_by_model}}};
use actix_web::{Result, HttpResponse, web::{Data, Json, block, ReqData, Path}, Error, http::header::ContentType};
use serde_json::json;
use crate::DBPool;
use crate::weapon::services::insert_weapon;
use crate::DBPooledConnection;
use uuid::Uuid;

use super::services::{insert_weapon_model, find_one_weapon_model_by_name, find_all_weapon_models, find_one_weapon_model_by_id, find_weapon_models_by_member, update_weapon_model, delete_weapon_model};

pub async fn create(weapon_model: Json<NewWeaponModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();
	let weapon_model_name: String = weapon_model.name.clone();
	let is_weapon_model_same_name_exists = block(move || find_one_weapon_model_by_name(weapon_model_name, &conn))
.await
.unwrap();

	match is_weapon_model_same_name_exists {
		Ok(_) => {
			return Ok(HttpResponse::Conflict().json(json!({ "message": "Weapon model with same name already exists" })));
		},
		Err(_) => {
			let weapon_model_inserted = block(move || insert_weapon_model(weapon_model.into_inner(), &db.get().unwrap()))
.await
.unwrap();

			match weapon_model_inserted {
				Ok(weapon_model_inserted) => {
					return Ok(HttpResponse::Created().content_type(ContentType::json()).json(weapon_model_inserted));
				},
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to insert weapon model: {}", err.to_string())})));
				}
			}
		}
	}
}

pub async fn list(db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let weapon_models = block(move || find_all_weapon_models(&conn))
.await
.unwrap();

	match weapon_models {
		Ok(weapon_models) => {
			return Ok(HttpResponse::Ok().content_type(ContentType::json()).json(weapon_models));
		},
		Err(_) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to get weapon models" })));
		}
	}
}

pub async fn buy(claims: ReqData<Claims>, weapon_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();
	let is_weapon_model_exists = block(move || find_one_weapon_model_by_id(&weapon_model_id.into_inner(), &conn))
		.await
		.unwrap();

	let weapon_model = match is_weapon_model_exists {
		Ok(weapon_model) => weapon_model,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Weapon model not found" }))),
	};

	let member_id = Uuid::parse_str(&claims.sub.clone()).unwrap();
	let sec_conn: DBPooledConnection = db.get().unwrap();
	let is_member_exists = block(move || find_one_member_by_id(member_id, &sec_conn))
		.await
		.unwrap();

	let member = match is_member_exists {
		Ok(member) => member,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Member not found" }))),
	};

	let fourth_conn: DBPooledConnection = db.get().unwrap();
	let is_weapon_model_already_owned = block(move || find_weapon_by_member_and_weapon_model(
		&member.member_id,
		&weapon_model.weapon_model_id,
		&fourth_conn
	))
		.await
		.unwrap();

	if is_weapon_model_already_owned.is_ok() {
		return Ok(HttpResponse::Conflict().json(json!({ "message": "Weapon model already owned" })));
	}

	if member.credits < weapon_model.price {
		return Ok(HttpResponse::BadRequest().json(json!({ "message": "Not enough credits" })));
	}

	let new_weapon = NewWeapon {
		member_id: member.member_id,
		weapon_model_id: weapon_model.weapon_model_id,
		health: weapon_model.max_health,
		ammo: weapon_model.max_ammo,
	};

	let fifth_conn: DBPooledConnection = db.get().unwrap();
	let insert_new_weapon = block(move || insert_weapon(
		new_weapon,
		&fifth_conn
	))
		.await
		.unwrap();

	if insert_new_weapon.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert new weapon" })));
	}

	let payment: i32 = member.credits - weapon_model.price;
	let third_conn: DBPooledConnection = db.get().unwrap();
	let _ = block(move || update_member_credits(member_id, payment, &third_conn))
		.await
		.unwrap();

	return Ok(HttpResponse::Ok().json(json!({ "message": "Successfully bought weapon model" })));
}

pub async fn retrieve_weapon_models_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let weapon_models_found = block(move || find_weapon_models_by_member(&member_id, &conn))
	.await
	.unwrap();

	match weapon_models_found {
		Ok(weapon_models_found) => Ok(HttpResponse::Ok().json(weapon_models_found)),
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving weapon model owned by member: {}", err.to_string()),
			})));
		},
	}
}

pub async fn update(weapon_model_id: Path<Uuid>, weapon_model: Json<NewWeaponModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let weapon_model_updated = block(move || update_weapon_model(&weapon_model_id.into_inner(), weapon_model.into_inner(), &conn))
		.await
		.unwrap();

	match weapon_model_updated {
		Ok(weapon_model_updated) => {
			return Ok(HttpResponse::Ok().json(json!(weapon_model_updated)));
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to update weapon model: {}", err.to_string())})));
		}
	}
}

pub async fn delete(weapon_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let weapon_model_id = weapon_model_id.into_inner();

	let weapon_conn = db.get().unwrap();
	let _ = block(move || delete_weapons_by_model(&weapon_model_id.clone(), &weapon_conn))
		.await
		.unwrap();

	let weapon_model_conn = db.get().unwrap();
	let weapon_model_deleted = block(move || delete_weapon_model(&weapon_model_id.clone(), &weapon_model_conn))
		.await
		.unwrap();

	match weapon_model_deleted {
		Ok(_) => {
			return Ok(HttpResponse::NoContent().finish());
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to delete weapon model: {}", err.to_string())})));
		}
	}
}