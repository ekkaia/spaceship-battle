use crate::weapon_model::models::NewWeaponModel;
use crate::DBPooledConnection;
use crate::schema::spaceship::weapon_model;
use actix_web::Result;
use diesel::{sql_query, sql_types, QueryDsl, result::Error, RunQueryDsl, ExpressionMethods};
use uuid::Uuid;

use super::models::WeaponModel;

pub fn insert_weapon_model(
    weapon_model: NewWeaponModel,
    conn: &DBPooledConnection,
) -> Result<WeaponModel, Error> {
    return diesel::insert_into(weapon_model::table)
        .values(weapon_model)
        .get_result(conn);
}

pub fn find_one_weapon_model_by_name(
    name: String,
    conn: &DBPooledConnection,
) -> Result<WeaponModel, Error> {
    return weapon_model::table
        .filter(weapon_model::name.eq(name))
        .first::<WeaponModel>(conn);
}

pub fn find_one_weapon_model_by_id(
    weapon_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<WeaponModel, Error> {
    return weapon_model::table
        .filter(weapon_model::weapon_model_id.eq(weapon_model_id))
        .first::<WeaponModel>(conn);
}

pub fn find_one_weapon_model(
    weapon_model_id: Uuid,
    conn: &DBPooledConnection,
) -> Result<WeaponModel, Error> {
    return weapon_model::table
        .filter(weapon_model::weapon_model_id.eq(weapon_model_id))
        .first::<WeaponModel>(conn);
}

pub fn find_all_weapon_models(
    conn: &DBPooledConnection,
) -> Result<Vec<WeaponModel>, Error> {
    return weapon_model::table.load::<WeaponModel>(conn);
}

pub fn find_weapon_models_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<WeaponModel>, Error> {
    let string_query = include_str!("queries/find_weapon_models_by_member.sql");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(member_id)
        .get_results(conn);
}

pub fn update_weapon_model(
	weapon_model_id: &Uuid,
	weapon_model: NewWeaponModel,
	conn: &DBPooledConnection,
) -> Result<WeaponModel, Error> {
	return diesel::update(weapon_model::table)
		.filter(weapon_model::weapon_model_id.eq(weapon_model_id))
		.set(weapon_model)
		.get_result(conn);
}

pub fn find_one_weapon_model_by_weapon_id(
    weapon_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<WeaponModel, Error> {
    return sql_query(
        "SELECT * FROM spaceship.weapon_model WHERE weapon_model_id = (SELECT weapon_model_id FROM spaceship.weapon WHERE weapon_id = $1)",
    )
        .bind::<sql_types::Uuid, _>(weapon_id)
        .get_result::<WeaponModel>(conn);
}

pub fn delete_weapon_model(
	weapon_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, Error> {
	let result = diesel::delete(weapon_model::table)
		.filter(weapon_model::weapon_model_id.eq(weapon_model_id))
		.execute(conn);
	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}