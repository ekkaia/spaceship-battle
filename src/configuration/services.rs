use diesel::{sql_query, sql_types, RunQueryDsl, OptionalExtension};
use uuid::Uuid;

use crate::DBPooledConnection;

use super::models::Configuration;

pub fn find_main_configuration_by_member(
    requested_member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Option<Configuration>, diesel::result::Error> {
    let string_query = include_str!("queries/find_configurations_by_member.sql")
        .replace("--!main_ship!", "");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(requested_member_id)
        .get_result(conn)
        .optional();
}

pub fn find_configurations_by_member(
    requested_member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<Configuration>, diesel::result::Error> {
    let string_query = include_str!("queries/find_configurations_by_member.sql");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(requested_member_id)
        .get_results(conn);
}

pub fn find_one_configuration_by_member(
    requested_member_id: &Uuid,
    requested_spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Option<Configuration>, diesel::result::Error> {
    let string_query = include_str!("queries/find_configurations_by_member.sql")
        .replace("--!find_one_spaceship!", "");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(requested_member_id)
        .bind::<sql_types::Uuid, _>(requested_spaceship_id)
        .get_result(conn);
}