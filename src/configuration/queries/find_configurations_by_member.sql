SELECT DISTINCT
"spaceship".spaceship_id,
"spaceship".name,
"spaceship".member_id,
"spaceship".health,
"spaceship_model".max_health,
"spaceship_model".spaceship_model_id,
"spaceship_model".name AS "model_name",
"spaceship_model".spaceship_type,
"spaceship_model".tank_capacity,
"spaceship_model".max_power,
"spaceship_model".consumption_for_power,
"spaceship_model".energy_repartition_delay,
"spaceship".is_main_ship,
JSONB_AGG(
	DISTINCT JSONB_BUILD_OBJECT(
		'module_zone_id', "module_zone".module_zone_id,
		'shielding', "module_zone".shielding
	)
) AS module_zones,
JSONB_AGG(
	JSONB_BUILD_OBJECT(
		'module_slot_id', "module_slot".module_slot_id,
		'module_zone_id', "module_slot".module_zone_id,
		'module_type', "module_slot".module_type,
		'size', "module_slot".size
	)
) AS module_slots,
JSONB_BUILD_OBJECT(
	'engine_id', "engine".engine_id,
	'name', "engine_model".name,
	'engine_model_id', "engine_model".engine_model_id,
	'module_slot_id', "engine".module_slot_id,
	'health', "engine".health,
	'max_health', "engine_model".max_health,
	'max_power', "engine_model".max_power,
	'max_speed', "engine_model".max_speed
) AS engine,
COALESCE(
	JSONB_AGG(
		DISTINCT JSONB_BUILD_OBJECT(
			'weapon_id', "weapon".weapon_id,
			'weapon_model_id', "weapon_model".weapon_model_id,
			'module_slot_id', "weapon".module_slot_id,
			'health', "weapon".health,
			'max_health', "weapon_model".max_health,
			'ammo', "weapon".ammo,
			'name', "weapon_model".name,
			'weapon_type', "weapon_model".weapon_type,
			'max_power', "weapon_model".max_power,
			'max_ammo', "weapon_model".max_ammo,
			'reload_time', "weapon_model".reload_time,
			'next_shot_time', '1970-01-01T00:00:00.000000Z',
			'damage', "weapon_model".damage,
			'damage_type', "weapon_model".damage_type,
			'ammo_type', "weapon_model".ammo_type,
			'travel_time_by_distance', "weapon_model".travel_time_by_distance,
			'min_shooting_distance', "weapon_model".min_shooting_distance,
			'max_shooting_distance', "weapon_model".max_shooting_distance
		)
	) FILTER (WHERE "weapon".weapon_id IS NOT NULL), '[]'
) AS weapons,
CASE 
	WHEN "shield".shield_id IS NULL THEN NULL
	ELSE JSONB_BUILD_OBJECT(
		'shield_id', "shield".shield_id,
		'shield_model_id', "shield_model".shield_model_id,
		'module_slot_id', "shield".module_slot_id,
		'health', "shield".health,
		'max_health', "shield_model".max_health,
		'name', "shield_model".name,
		'shield_type', "shield_model".shield_type,
		'shield_type_protection', "shield_model".shield_type_protection,
		'reload_time', "shield_model".reload_time,
		'max_power', "shield_model".max_power
	)
END AS shield,
CASE 
	WHEN "cargo".cargo_id IS NULL THEN NULL
	ELSE JSONB_BUILD_OBJECT(
		'cargo_id', "cargo".cargo_id,
		'cargo_model_id', "cargo_model".cargo_model_id,
		'module_slot_id', "cargo".module_slot_id,
		'health', "cargo".health,
		'max_health', "cargo_model".max_health,
		'name', "cargo_model".name,
		'cargo_type', "cargo_model".cargo_type,
		'capacity', "cargo_model".capacity
	)
END AS cargo
FROM "spaceship".spaceship
JOIN "spaceship".spaceship_model ON "spaceship_model".spaceship_model_id = "spaceship".spaceship_model_id
JOIN "spaceship".module_zone ON "module_zone".spaceship_model_id = "spaceship".spaceship_model_id
JOIN "spaceship".module_slot ON "module_slot".module_zone_id = "module_zone".module_zone_id
JOIN "spaceship".engine ON "engine".spaceship_id = "spaceship".spaceship_id
JOIN "spaceship".engine_model ON "engine".engine_model_id = "engine_model".engine_model_id
LEFT JOIN "spaceship".weapon ON "weapon".spaceship_id = "spaceship".spaceship_id
LEFT JOIN "spaceship".weapon_model ON "weapon".weapon_model_id = "weapon_model".weapon_model_id
LEFT JOIN "spaceship".shield ON "shield".spaceship_id = "spaceship".spaceship_id
LEFT JOIN "spaceship".shield_model ON "shield_model".shield_model_id = "shield".shield_model_id
LEFT JOIN "spaceship".cargo ON "cargo".spaceship_id = "spaceship".spaceship_id
LEFT JOIN "spaceship".cargo_model ON "cargo_model".cargo_model_id = "cargo".cargo_model_id
WHERE "spaceship".member_id = $1
AND "spaceship".name IS NOT NULL
--!main_ship! AND "spaceship".is_main_ship IS true
--!find_one_spaceship! AND "spaceship".spaceship_id = $2
GROUP BY
	"spaceship".spaceship_id,
	"spaceship_model".max_health,
	"spaceship_model".spaceship_model_id,
	model_name,
	"spaceship_model".spaceship_type,
	"spaceship_model".tank_capacity,
	"spaceship_model".max_power,
	"spaceship_model".consumption_for_power,
	"spaceship_model".energy_repartition_delay,
	"engine".engine_id,
	"engine_model".name,
	"engine_model".engine_model_id,
	"shield".shield_id,
	"shield_model".shield_model_id,
	"cargo".cargo_id,
	"cargo_model".cargo_model_id
--!find_one_spaceship! LIMIT 1;