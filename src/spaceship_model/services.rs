use crate::schema::spaceship::{spaceship_model, module_slot, module_zone};
use crate::DBPooledConnection;
use actix_web::Result;
use diesel::{sql_query, sql_types, result::Error, ExpressionMethods, RunQueryDsl, QueryDsl};
use uuid::Uuid;

use super::models::{SpaceshipModel, NewSpaceshipModel, NewModuleZone, NewModuleSlot, LightSpaceshipModel, ModuleZone, ModuleSlot};

pub fn insert_module_zone(
	module_zone: NewModuleZone,
	conn: &DBPooledConnection,
) -> Result<ModuleZone, Error> {
	return diesel::insert_into(module_zone::table)
		.values(module_zone)
		.get_result(conn);
}

pub fn insert_module_slot(
	module_slot: NewModuleSlot,
	conn: &DBPooledConnection,
) -> Result<ModuleSlot, Error> {
	return diesel::insert_into(module_slot::table)
		.values(module_slot)
		.get_result(conn);
}

pub fn insert_spaceship_model(
	spaceship_model: NewSpaceshipModel,
	conn: &DBPooledConnection,
) -> Result<LightSpaceshipModel, Error> {
	return diesel::insert_into(spaceship_model::table)
		.values(spaceship_model)
		.get_result(conn);
}

pub fn find_module_zone_by_model(
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Vec<ModuleZone>, Error> {
	return module_zone::table
		.filter(module_zone::spaceship_model_id.eq(spaceship_model_id))
		.load(conn);
}

pub fn find_module_slot_by_zone(
	module_zone_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Vec<ModuleSlot>, Error> {
	return module_slot::table
		.filter(module_slot::module_zone_id.eq(module_zone_id))
		.load(conn);
}

pub fn find_one_spaceship_model_by_name(
	name: String,
	conn: &DBPooledConnection,
) -> Result<LightSpaceshipModel, Error> {
	return spaceship_model::table
		.filter(spaceship_model::name.eq(name))
		.first(conn);
}

pub fn find_one_spaceship_model_by_id(
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<SpaceshipModel, Error> {
	let string_query =
		include_str!("queries/find_spaceship_models.sql").replace("--!find_by_id!", "");

	return sql_query(string_query)
		.bind::<sql_types::Uuid, _>(spaceship_model_id)
		.get_result(conn);
}

pub fn find_all_spaceship_models(
	conn: &DBPooledConnection,
) -> Result<Vec<SpaceshipModel>, Error> {
	let string_query =
		include_str!("queries/find_spaceship_models.sql").replace("--!find_all!", "");

	return sql_query(string_query).get_results(conn);
}

pub fn find_spaceship_models_by_member(
	member_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<Vec<SpaceshipModel>, Error> {
	let string_query =
		include_str!("queries/find_spaceship_models.sql").replace("--!find_by_member!", "");

	return sql_query(string_query)
		.bind::<sql_types::Uuid, _>(member_id)
		.get_results(conn);
}

pub fn update_module_slot_by_id(
	module_slot_id: &Uuid,
	module_slot: NewModuleSlot,
	conn: &DBPooledConnection,
) -> Result<ModuleSlot, Error> {
	return diesel::update(module_slot::table)
		.filter(module_slot::module_slot_id.eq(module_slot_id))
		.set(module_slot)
		.get_result(conn);
}

pub fn update_module_zone_by_id(
	module_zone_id: &Uuid,
	module_zone: NewModuleZone,
	conn: &DBPooledConnection,
) -> Result<ModuleZone, Error> {
	return diesel::update(module_zone::table)
		.filter(module_zone::module_zone_id.eq(module_zone_id))
		.set(module_zone)
		.get_result(conn);
}

pub fn update_spaceship_model_by_id(
	spaceship_model_id: &Uuid,
	spaceship_model: NewSpaceshipModel,
	conn: &DBPooledConnection,
) -> Result<LightSpaceshipModel, Error> {
	return diesel::update(spaceship_model::table)
		.filter(spaceship_model::spaceship_model_id.eq(spaceship_model_id))
		.set(spaceship_model)
		.get_result(conn);
}

pub fn delete_module_slot_by_zone(
	module_zone_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, Error> {
	return diesel::delete(module_slot::table)
		.filter(module_slot::module_zone_id.eq(module_zone_id))
		.execute(conn);
}

pub fn delete_module_zone_by_model(
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, Error> {
	return diesel::delete(module_zone::table)
		.filter(module_zone::spaceship_model_id.eq(spaceship_model_id))
		.execute(conn);
}

pub fn delete_spaceship_model(
	spaceship_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, Error> {
	return diesel::delete(spaceship_model::table)
		.filter(spaceship_model::spaceship_model_id.eq(spaceship_model_id))
		.execute(conn);
}