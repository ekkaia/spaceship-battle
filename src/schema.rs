pub mod spaceship {
	table! {
		use diesel::sql_types::*;

		spaceship.cargo (cargo_id) {
			cargo_id -> Uuid,
			spaceship_id -> Nullable<Uuid>,
			cargo_model_id -> Uuid,
			member_id -> Uuid,
			module_slot_id -> Nullable<Uuid>,
			health -> Int4,
			created_at -> Nullable<Timestamptz>,
			updated_at -> Nullable<Timestamptz>,
		}
	}

	table! {
		use diesel::sql_types::*;
		use crate::cargo_model::models::CargoTypeMapping;

		spaceship.cargo_model (cargo_model_id) {
			cargo_model_id -> Uuid,
			name -> Text,
			cargo_type -> CargoTypeMapping,
			max_health -> Int4,
			capacity -> Int4,
			size -> Int4,
			price -> Int4,
		}
	}

	table! {
		use diesel::sql_types::*;

		spaceship.engine (engine_id) {
			engine_id -> Uuid,
			spaceship_id -> Nullable<Uuid>,
			engine_model_id -> Uuid,
			member_id -> Uuid,
			module_slot_id -> Nullable<Uuid>,
			health -> Int4,
			created_at -> Nullable<Timestamptz>,
			updated_at -> Nullable<Timestamptz>,
		}
	}

	table! {
		use diesel::sql_types::*;
		use crate::engine_model::models::EngineTypeMapping;

		spaceship.engine_model (engine_model_id) {
			engine_model_id -> Uuid,
			name -> Text,
			engine_type -> EngineTypeMapping,
			max_power -> Int4,
			max_speed -> Int4,
			max_health -> Int4,
			size -> Int4,
			price -> Int4,
		}
	}

	table! {
		use diesel::sql_types::*;

		spaceship.member (member_id) {
			member_id -> Uuid,
			name -> Text,
			email -> Text,
			credits -> Int4,
			password -> Text,
			is_admin -> Bool,
			created_at -> Nullable<Timestamptz>,
			updated_at -> Nullable<Timestamptz>,
		}
	}

	table! {
		use diesel::sql_types::*;
		use crate::configuration::models::ModuleTypeMapping;

		spaceship.module_slot (module_slot_id) {
			module_slot_id -> Uuid,
			module_zone_id -> Uuid,
			module_type -> ModuleTypeMapping,
			size -> Int4,
		}
	}

	table! {
		use diesel::sql_types::*;

		spaceship.module_zone (module_zone_id) {
			module_zone_id -> Uuid,
			spaceship_model_id -> Uuid,
			shielding -> Int4,
		}
	}

	table! {
		use diesel::sql_types::*;

		spaceship.shield (shield_id) {
			shield_id -> Uuid,
			spaceship_id -> Nullable<Uuid>,
			shield_model_id -> Uuid,
			member_id -> Uuid,
			module_slot_id -> Nullable<Uuid>,
			health -> Int4,
			created_at -> Nullable<Timestamptz>,
			updated_at -> Nullable<Timestamptz>,
		}
	}

	table! {
		use diesel::sql_types::*;
		use crate::shield_model::models::ShieldTypeMapping;

		spaceship.shield_model (shield_model_id) {
			shield_model_id -> Uuid,
			name -> Text,
			shield_type -> ShieldTypeMapping,
			shield_type_protection -> Nullable<Int4>,
			max_health -> Int4,
			reload_time -> Int4,
			max_power -> Int4,
			size -> Int4,
			price -> Int4,
		}
	}

	table! {
		use diesel::sql_types::*;

		spaceship.spaceship (spaceship_id) {
			spaceship_id -> Uuid,
			spaceship_model_id -> Uuid,
			name -> Nullable<Text>,
			member_id -> Uuid,
			health -> Int4,
			is_main_ship -> Bool,
			created_at -> Nullable<Timestamptz>,
			updated_at -> Nullable<Timestamptz>,
		}
	}

	table! {
		use diesel::sql_types::*;
		use crate::spaceship_model::models::SpaceshipTypeMapping;

		spaceship.spaceship_model (spaceship_model_id) {
			spaceship_model_id -> Uuid,
			name -> Text,
			max_health -> Int4,
			spaceship_type -> SpaceshipTypeMapping,
			tank_capacity -> Int4,
			max_power -> Int4,
			consumption_for_power -> Int4,
			energy_repartition_delay -> Int4,
			price -> Int4,
		}
	}

	table! {
		use diesel::sql_types::*;

		spaceship.weapon (weapon_id) {
			weapon_id -> Uuid,
			spaceship_id -> Nullable<Uuid>,
			weapon_model_id -> Uuid,
			module_slot_id -> Nullable<Uuid>,
			member_id -> Uuid,
			health -> Int4,
			ammo -> Int4,
			created_at -> Nullable<Timestamptz>,
			updated_at -> Nullable<Timestamptz>,
		}
	}

	table! {
		use diesel::sql_types::*;
		use crate::weapon_model::models::{DamageTypeMapping, WeaponTypeMapping, AmmoTypeMapping};

		spaceship.weapon_model (weapon_model_id) {
			weapon_model_id -> Uuid,
			name -> Text,
			weapon_type -> WeaponTypeMapping,
			max_power -> Int4,
			max_ammo -> Int4,
			max_health -> Int4,
			reload_time -> Int4,
			damage -> Int4,
			damage_type -> DamageTypeMapping,
			ammo_type -> AmmoTypeMapping,
			travel_time_by_distance -> Int4,
			min_shooting_distance -> Int4,
			max_shooting_distance -> Int4,
			size -> Int4,
			price -> Int4,
		}
	}

	joinable!(cargo -> cargo_model (cargo_model_id));
	joinable!(cargo -> member (member_id));
	joinable!(cargo -> module_slot (module_slot_id));
	joinable!(cargo -> spaceship (spaceship_id));
	joinable!(engine -> engine_model (engine_model_id));
	joinable!(engine -> member (member_id));
	joinable!(engine -> module_slot (module_slot_id));
	joinable!(engine -> spaceship (spaceship_id));
	joinable!(module_slot -> module_zone (module_zone_id));
	joinable!(module_zone -> spaceship_model (spaceship_model_id));
	joinable!(shield -> member (member_id));
	joinable!(shield -> module_slot (module_slot_id));
	joinable!(shield -> shield_model (shield_model_id));
	joinable!(shield -> spaceship (spaceship_id));
	joinable!(spaceship -> member (member_id));
	joinable!(spaceship -> spaceship_model (spaceship_model_id));
	joinable!(weapon -> member (member_id));
	joinable!(weapon -> module_slot (module_slot_id));
	joinable!(weapon -> spaceship (spaceship_id));
	joinable!(weapon -> weapon_model (weapon_model_id));

	allow_tables_to_appear_in_same_query!(
		cargo,
		cargo_model,
		engine,
		engine_model,
		member,
		module_slot,
		module_zone,
		shield,
		shield_model,
		spaceship,
		spaceship_model,
		weapon,
		weapon_model,
	);
}
