SELECT
    "cargo_model".cargo_model_id,
	"cargo_model".name,
	"cargo_model".cargo_type,
	"cargo_model".max_health,
	"cargo_model".capacity,
	"cargo_model".size,
	"cargo_model".price
FROM "spaceship".cargo
JOIN "spaceship".cargo_model ON "cargo".cargo_model_id = "cargo_model".cargo_model_id
WHERE "cargo".member_id = $1