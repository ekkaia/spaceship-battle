use crate::schema::spaceship::cargo_model;
use diesel_derive_enum::DbEnum;
use serde::{Serialize, Deserialize};
use diesel::{Insertable, Queryable};
use uuid::Uuid;

#[derive(Debug, DbEnum, Serialize, Deserialize, Clone)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum CargoType {
	Basic,
	RadiationProtection,
}

#[derive(Debug, Queryable, QueryableByName, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[table_name = "cargo_model"]
pub struct CargoModel {
	pub cargo_model_id: Uuid,
	pub name: String,
	pub cargo_type: CargoType,
	pub max_health: i32,
	pub capacity: i32,
	pub size: i32,
	pub price: i32,
}

#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "cargo_model"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewCargoModel {
	pub name: String,
	pub max_health: i32,
	pub cargo_type: CargoType,
	pub capacity: i32,
	pub size: i32,
	pub price: i32,
}