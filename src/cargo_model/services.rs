use crate::cargo_model::models::NewCargoModel;
use crate::diesel::{ExpressionMethods, result::Error, RunQueryDsl, sql_query, sql_types, QueryDsl};
use crate::DBPooledConnection;
use crate::schema::spaceship::cargo_model;
use actix_web::Result;
use uuid::Uuid;

use super::models::CargoModel;

pub fn insert_cargo_model(
    cargo_model: NewCargoModel,
    conn: &DBPooledConnection,
) -> Result<CargoModel, Error> {
    return diesel::insert_into(cargo_model::table)
        .values(cargo_model)
        .get_result(conn);
}

pub fn find_one_cargo_model_by_name(
    name: String,
    conn: &DBPooledConnection,
) -> Result<CargoModel, Error> {
    return cargo_model::table
        .filter(cargo_model::name.eq(name))
        .first::<CargoModel>(conn);
}

pub fn find_one_cargo_model_by_id(
    cargo_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<CargoModel, Error> {
    return cargo_model::table
        .filter(cargo_model::cargo_model_id.eq(cargo_model_id))
        .first::<CargoModel>(conn);
}

pub fn find_one_cargo_model_by_cargo_id(
    cargo_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<CargoModel, Error> {
    return sql_query(
        "SELECT * FROM spaceship.cargo_model WHERE cargo_model_id = (SELECT cargo_model_id FROM spaceship.cargo WHERE cargo_id = $1)",
    )
        .bind::<sql_types::Uuid, _>(cargo_id)
        .get_result::<CargoModel>(conn);
}

pub fn find_all_cargo_models(
    conn: &DBPooledConnection,
) -> Result<Vec<CargoModel>, Error> {
    return cargo_model::table.load::<CargoModel>(conn);
}

pub fn find_cargo_models_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<CargoModel>, Error> {
    let string_query = include_str!("queries/find_cargo_models_by_member.sql");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(member_id)
        .get_results(conn);
}

pub fn update_cargo_model(
	cargo_model_id: Uuid,
	cargo_model: NewCargoModel,
	conn: &DBPooledConnection,
) -> Result<CargoModel, Error> {
	return diesel::update(cargo_model::table)
		.filter(cargo_model::cargo_model_id.eq(cargo_model_id))
		.set(cargo_model)
		.get_result(conn);
}

pub fn delete_cargo_model(
    cargo_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::delete(cargo_model::table)
        .filter(cargo_model::cargo_model_id.eq(cargo_model_id))
        .execute(conn);
    if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}