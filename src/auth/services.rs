use crate::auth::models::Claims;
use actix_http::HttpMessage;
use actix_web::{Error, error::ErrorUnauthorized, error::ErrorInternalServerError};
use actix_web::dev::ServiceRequest;
use jsonwebtoken::{decode, DecodingKey, Validation, EncodingKey, encode, Header};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use serde_json::json;

use crate::constants::SECRET_KEY;

use super::models::TokenType;

pub async fn check_login(req: ServiceRequest, credentials: BearerAuth) -> Result<ServiceRequest, (Error, ServiceRequest)> {
	let result = token_decoding(credentials.token().to_string());
	match result {
		Ok(claims) => {
			if claims.access_type == TokenType::AccessToken {
				req.extensions_mut().insert(claims);
				return Ok(req);
			}
			return Err((ErrorUnauthorized(json!({
				"message": "Wrong token type"
			})), req));
		}
		Err(e) => {
			return Err((e, req));
		}	
	}
}

pub async fn check_admin(req: ServiceRequest, credentials: BearerAuth) -> Result<ServiceRequest, (Error, ServiceRequest)> {
	let result = token_decoding(credentials.token().to_string());
	match result {
		Ok(claims) => {
			if claims.is_admin && claims.access_type == TokenType::AccessToken{
				req.extensions_mut().insert(claims);
				return Ok(req);
			}
			return Err((ErrorUnauthorized(json!({
				"message": "You are not an admin or wrong token type"
			})), req));
		}
		Err(e) => {
			return Err((e, req));
		}	
	}
}

pub fn token_decoding(token: String) -> Result<Claims, Error> {
	let decoding_key = &DecodingKey::from_secret(SECRET_KEY.as_ref());
	return decode::<Claims>(&token, decoding_key, &Validation::default())
		.map(|data| data.claims)
		.map_err(|e| ErrorUnauthorized(e.to_string()));
}

pub fn token_encoding(claims: Claims) -> Result<String, Error> {
	let encoding_key = &EncodingKey::from_secret(SECRET_KEY.as_ref());
	return encode(&Header::default(), &claims, encoding_key)
		.map(|token| token)
		.map_err(|e| ErrorInternalServerError(e.to_string()));
}