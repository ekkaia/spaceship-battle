use crate::schema::spaceship::member;
use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum TokenType {
	AccessToken,
	WebSocketToken,
}

#[derive(Debug, Insertable, Deserialize, Serialize)]
#[table_name = "member"]
pub struct Register {
	pub name: String,
	pub password: String,
	pub email: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Login {
	pub email: String,
	pub password: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Claims {
	pub sub: String,
	pub exp: i64,
	pub is_admin: bool,
	pub access_type: TokenType,
}
