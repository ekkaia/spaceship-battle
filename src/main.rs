pub mod auth;
pub mod cargo;
pub mod cargo_model;
pub mod common;
pub mod configuration;
pub mod constants;
pub mod engine;
pub mod engine_model;
pub mod game;
pub mod member;
pub mod schema;
pub mod shield;
pub mod shield_model;
pub mod spaceship;
pub mod spaceship_model;
pub mod weapon;
pub mod weapon_model;

#[macro_use]
extern crate diesel;

use crate::auth::services::{check_admin, check_login};
use actix::Actor;
use actix_cors::Cors;
use actix_web::{
    middleware,
    web::{delete, get, post, put, scope, Data},
    App, HttpServer,
};
use actix_web_httpauth::middleware::HttpAuthentication;
use constants::{DATABASE_URL, HOST, PORT, SENTRY_KEY};
use diesel::{r2d2::ConnectionManager, r2d2::Pool, PgConnection};
use dotenv::dotenv;
use game::models::game_server::GameServer;
use r2d2::PooledConnection;
use std::{env, io};

pub type DBPool = Pool<ConnectionManager<PgConnection>>;
pub type DBPooledConnection = PooledConnection<ConnectionManager<PgConnection>>;

#[actix_web::main]
async fn main() -> io::Result<()> {
    dotenv().ok();
    env::set_var("RUST_BACKTRACE", "1");
    env::set_var("RUST_LOG", "actix_web=debug,actix_server=info");
    env_logger::init();

    let _guard = match SENTRY_KEY.as_ref() {
        Option::Some(sentry_key) => Option::Some(sentry::init((
            sentry_key.to_string(),
            sentry::ClientOptions {
                release: sentry::release_name!(),
                traces_sample_rate: 1.0,
                ..Default::default()
            },
        ))),
        Option::None => {
            println!("_guard isn't initialized because SENTRY_KEY is missing");
            Option::None
        }
    };

    let manager: ConnectionManager<PgConnection> =
        ConnectionManager::<PgConnection>::new(DATABASE_URL.to_string());

    let pool: Pool<ConnectionManager<PgConnection>> = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool");

    let server = GameServer::new().start();

    return HttpServer::new(move || {
		// !SEULEMENT EN DEV POUR TESTER
		let cors = Cors::permissive();

		App::new()
			.app_data(Data::new(pool.clone()))
			.app_data(Data::new(server.clone()))
			.wrap(cors)
			.wrap(middleware::Logger::default())
			.wrap(sentry_actix::Sentry::new())

			.service(
				scope("/api")
				.service(
					scope("/auth")
						.route("/login", post().to(auth::routes::login))
						.route("/register", post().to(auth::routes::register))
						.route("/websocket_login", get()
							.to(auth::routes::websocket_login)
							.wrap(HttpAuthentication::bearer(check_login))
						),
				)
				.service(
					scope("/cargo_models")
						.route("/{cargo_models_id}/buy", get()
							.to(cargo_model::routes::buy)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", get()
							.to(cargo_model::routes::list)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", post()
							.to(cargo_model::routes::create)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{cargo_models_id}", put()
							.to(cargo_model::routes::update)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{cargo_models_id}", delete()
							.to(cargo_model::routes::delete)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
				)
				.service(
					scope("/engine_models")
						.route("/{engine_models_id}/buy", get()
							.to(engine_model::routes::buy)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", get()
							.to(engine_model::routes::list)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", post()
							.to(engine_model::routes::create)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{engine_models_id}", put()
							.to(engine_model::routes::update)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{engine_models_id}", delete()
							.to(engine_model::routes::delete)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
				)
				.service(
					scope("/shield_models")
						.route("/{shield_models_id}/buy", get()
							.to(shield_model::routes::buy)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", get()
							.to(shield_model::routes::list)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", post()
							.to(shield_model::routes::create)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{shield_models_id}", put()
							.to(shield_model::routes::update)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{shield_models_id}", delete()
							.to(shield_model::routes::delete)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
				)
				.service(
					scope("/weapon_models")
						.route("/{weapon_models_id}/buy", get().to(weapon_model::routes::buy).wrap(HttpAuthentication::bearer(check_login)))
						.route("", get()
							.to(weapon_model::routes::list)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", post()
							.to(weapon_model::routes::create)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{weapon_models_id}", put()
							.to(weapon_model::routes::update)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{weapon_models_id}", delete()
							.to(weapon_model::routes::delete)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
				)
				.service(
					scope("/spaceship_models")
						.route("/{spaceship_models_id}/buy", get()
							.to(spaceship_model::routes::buy)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", get()
							.to(spaceship_model::routes::list)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", post()
							.to(spaceship_model::routes::create)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{spaceship_models_id}", delete()
							.to(spaceship_model::routes::delete)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
				)
				.service(
					scope("/room")
						.route("", get()
							.to(game::routes::room)
						)
				)
				.service(
					scope("/members/me")
						.route("", get()
							.to(member::routes::retrieve_member_me)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("", delete()
							.to(member::routes::delete_member_me)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/configurations/main", get()
							.to(configuration::routes::retrieve_main_configuration_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/configurations/{configuration_id}", delete()
							.to(configuration::routes::delete_configuration_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/configurations", post()
							.to(configuration::routes::create_configuration_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/configurations", get()
							.to(configuration::routes::retrieve_configurations_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/spaceships", get()
							.to(spaceship::routes::retrieve_spaceships_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/engines", get()
							.to(engine::routes::retrieve_engines_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/weapons", get()
							.to(weapon::routes::retrieve_weapons_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/cargos", get()
							.to(cargo::routes::retrieve_cargos_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/shields", get()
							.to(shield::routes::retrieve_shields_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/shield_models", get()
							.to(shield_model::routes::retrieve_shield_models_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/cargo_models", get()
							.to(cargo_model::routes::retrieve_cargo_models_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/weapon_models", get()
							.to(weapon_model::routes::retrieve_weapon_models_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/engine_models", get()
							.to(engine_model::routes::retrieve_engine_models_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
						.route("/spaceship_models", get()
							.to(spaceship_model::routes::retrieve_spaceship_models_by_member)
							.wrap(HttpAuthentication::bearer(check_login))
						)
				)
				.service(
					scope("/members")
						.route("/{member_id}", delete()
							.to(member::routes::delete_member_by_id)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("/{member_id}", put()
							.to(member::routes::update_member_by_id)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
						.route("", get()
							.to(member::routes::retrieve_members)
							.wrap(HttpAuthentication::bearer(check_admin))
						)
				)
			)
	})
	.bind(format!("{}:{}", HOST.to_string(), PORT.to_string()))?
	.run()
	.await;
}
