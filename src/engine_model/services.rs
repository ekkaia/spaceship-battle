use crate::diesel::ExpressionMethods;
use crate::engine_model::models::NewEngineModel;
use crate::DBPooledConnection;
use crate::schema::spaceship::engine_model;
use actix_web::{web::Json, Result};
use diesel::{sql_query, sql_types, QueryDsl, result::Error, RunQueryDsl};
use uuid::Uuid;

use super::models::EngineModel;

pub fn insert_engine_model(
    engine_model: Json<NewEngineModel>,
    conn: &DBPooledConnection,
) -> Result<EngineModel, diesel::result::Error> {
    return diesel::insert_into(engine_model::table)
        .values(engine_model.into_inner())
        .get_result(conn);
}

pub fn find_one_engine_model_by_name(
    name: String,
    conn: &DBPooledConnection,
) -> Result<EngineModel, diesel::result::Error> {
    return engine_model::table
        .filter(engine_model::name.eq(name))
        .first::<EngineModel>(conn);
}

pub fn find_one_engine_model_by_id(
    engine_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<EngineModel, diesel::result::Error> {
    return engine_model::table
        .filter(engine_model::engine_model_id.eq(&engine_model_id))
        .first::<EngineModel>(conn);
}

pub fn find_one_engine_model_by_engine_id(
    engine_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<EngineModel, diesel::result::Error> {
    return sql_query(
        "SELECT * FROM spaceship.engine_model WHERE engine_model_id = (SELECT engine_model_id FROM spaceship.engine WHERE engine_id = $1)",
    )
        .bind::<sql_types::Uuid, _>(engine_id)
        .get_result::<EngineModel>(conn);
}

pub fn find_all_engine_models(
    conn: &DBPooledConnection,
) -> Result<Vec<EngineModel>, diesel::result::Error> {
    return engine_model::table.load::<EngineModel>(conn);
}

pub fn find_engine_models_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<EngineModel>, diesel::result::Error> {
    let string_query = include_str!("queries/find_engine_models_by_member.sql");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(member_id)
        .get_results(conn);
}

pub fn update_engine_model(
	engine_model_id: Uuid,
	engine_model: NewEngineModel,
	conn: &DBPooledConnection,
) -> Result<EngineModel, Error> {
	return diesel::update(engine_model::table)
		.filter(engine_model::engine_model_id.eq(engine_model_id))
		.set(engine_model)
		.get_result(conn);
}

pub fn delete_engine_model(
    engine_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::delete(engine_model::table)
        .filter(engine_model::engine_model_id.eq(engine_model_id))
        .execute(conn);
    if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}
