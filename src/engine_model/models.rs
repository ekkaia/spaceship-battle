use crate::schema::spaceship::engine_model;
use diesel_derive_enum::DbEnum;
use serde::{Serialize, Deserialize};
use diesel::{Insertable, Queryable};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, DbEnum, Deserialize)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum EngineType {
	Basic,
}

#[derive(Debug, Queryable, QueryableByName, Deserialize, Serialize)]
#[table_name = "engine_model"]
#[serde(rename_all = "camelCase")]
pub struct EngineModel {
	pub engine_model_id: Uuid,
	pub name: String,
	pub engine_type: EngineType,
	pub max_health: i32,
	pub max_power: i32,
	pub max_speed: i32,
	pub size: i32,
	pub price: i32,
}

#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "engine_model"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewEngineModel {
	pub name: String,
	pub max_health: i32,
	pub max_power: i32,
	pub max_speed: i32,
	pub engine_type: EngineType,
	pub size: i32,
	pub price: i32,
}