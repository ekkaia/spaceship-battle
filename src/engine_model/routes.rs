use crate::{engine_model::models::NewEngineModel, auth::models::Claims, member::services::{find_one_member_by_id, update_member_credits}, engine::{models::NewEngine, services::{insert_engine, find_engine_by_member_and_engine_model, delete_engines_by_model}}};
use actix_web::{Result, HttpResponse, web::{Data, Json, block, ReqData, Path}, Error, http::header::ContentType};
use serde_json::json;
use crate::DBPool;
use uuid::Uuid;
use crate::DBPooledConnection;

use super::services::{insert_engine_model, find_one_engine_model_by_name, find_all_engine_models, find_one_engine_model_by_id, find_engine_models_by_member, update_engine_model, delete_engine_model};

pub async fn create(engine_model: Json<NewEngineModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let engine_model_name: String = engine_model.name.clone();

	let is_engine_model_same_name_exists = block(move || find_one_engine_model_by_name(engine_model_name, &conn))
.await
.unwrap();

	match is_engine_model_same_name_exists {
		Ok(_) => {
			return Ok(HttpResponse::Conflict().json(json!({
				"message": "Engine model with same name already exists",
			})));
		},
		Err(_) => {
			let engine_model_inserted = block(move || insert_engine_model(engine_model, &db.get().unwrap()))
.await
.unwrap();

			match engine_model_inserted {
				Ok(engine_model_inserted) => {
					return Ok(HttpResponse::Created().content_type(ContentType::json()).json(engine_model_inserted));
				},
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({
						"message": format!("Failed to insert engine model: {}", err.to_string()),
					})));
				}
			}
		}
	}
}

pub async fn list(db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let engine_models = block(move || find_all_engine_models(&conn))
.await
.unwrap();

	match engine_models {
		Ok(engine_models) => {
			return Ok(HttpResponse::Ok().content_type(ContentType::json()).json(engine_models));
		},
		Err(_) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to get engine models" })));
		}
	}
}

pub async fn buy(claims: ReqData<Claims>, engine_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();
	let is_engine_model_exists = block(move || find_one_engine_model_by_id(&engine_model_id.into_inner(), &conn))
		.await
		.unwrap();

	let engine_model = match is_engine_model_exists {
		Ok(engine_model) => engine_model,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Engine model not found" }))),
	};

	let member_id = Uuid::parse_str(&claims.sub.clone()).unwrap();
	let sec_conn: DBPooledConnection = db.get().unwrap();
	let is_member_exists = block(move || find_one_member_by_id(member_id, &sec_conn))
		.await
		.unwrap();

	let member = match is_member_exists {
		Ok(member) => member,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Member not found" }))),
	};

	if member.credits < engine_model.price {
		return Ok(HttpResponse::BadRequest().json(json!({ "message": "Not enough credits" })));
	}

	let fourth_conn: DBPooledConnection = db.get().unwrap();
	let is_engine_model_already_owned = block(move || find_engine_by_member_and_engine_model(
		&member.member_id,
		&engine_model.engine_model_id,
		&fourth_conn
	))
		.await
		.unwrap();

	if is_engine_model_already_owned.is_ok() {
		return Ok(HttpResponse::Conflict().json(json!({ "message": "Engine model already owned" })));
	}

	let new_engine = NewEngine {
		engine_model_id: engine_model.engine_model_id,
		member_id: member.member_id,
		health: engine_model.max_health,
	};

	let fifth_conn: DBPooledConnection = db.get().unwrap();
	let insert_new_engine = block(move || insert_engine(
		new_engine,
		&fifth_conn
	))
		.await
		.unwrap();

	if insert_new_engine.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert new engine" })));
	}

	let payment: i32 = member.credits - engine_model.price;
	let third_conn: DBPooledConnection = db.get().unwrap();
	let _ = block(move || update_member_credits(member_id, payment, &third_conn))
		.await
		.unwrap();

	return Ok(HttpResponse::Ok().json(json!({ "message": "Successfully bought engine model" })));
}

pub async fn retrieve_engine_models_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let engine_models_found = block(move || find_engine_models_by_member(&member_id, &conn))
		.await
		.unwrap();

	match engine_models_found {
		Ok(engine_models_found) => Ok(HttpResponse::Ok().json(engine_models_found)),
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving engine model owned by member: {}", err.to_string()),
			})));
		},
	}
}

pub async fn update(engine_model_id: Path<Uuid>, engine_model: Json<NewEngineModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let engine_model_updated = block(move || update_engine_model(engine_model_id.into_inner(), engine_model.into_inner(), &conn))
		.await
		.unwrap();

	match engine_model_updated {
		Ok(engine_model_updated) => {
			return Ok(HttpResponse::Ok().json(json!(engine_model_updated)));
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to update engine model: {}", err.to_string())})));
		}
	}
}

pub async fn delete(engine_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let engine_model_id = engine_model_id.into_inner();

	let engine_conn = db.get().unwrap();
	let _ = block(move || delete_engines_by_model(&engine_model_id.clone(), &engine_conn))
		.await
		.unwrap();

	let engine_model_conn = db.get().unwrap();
	let engine_model_deleted = block(move || delete_engine_model(&engine_model_id.clone(), &engine_model_conn))
		.await
		.unwrap();

	match engine_model_deleted {
		Ok(_) => {
			return Ok(HttpResponse::NoContent().finish());
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to delete engine model: {}", err.to_string())})));
		}
	}
}