SELECT
    "engine_model".engine_model_id,
	"engine_model".name,
	"engine_model".max_health,
	"engine_model".engine_type,
	"engine_model".price,
	"engine_model".size,
    "engine_model".max_power,
    "engine_model".max_speed
FROM "spaceship".engine
JOIN "spaceship".engine_model ON "engine".engine_model_id = "engine_model".engine_model_id
WHERE "engine".member_id = $1