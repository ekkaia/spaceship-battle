# Load testing with Locust

[Locust](https://locust.io) is an open-source load testing tool that emulates the simultaneous use of the game API by several users. 

## Usage

Ensure that you have Python 3 installed on your system. Then, install Locust with

```
pip install locust
```

To start testing, use the following command:

```
locust -f load.py
```

This will launch the Locust web interface on [localhost:8089](http://0.0.0.0:8089). Select how much users you want to simulate, as well as their spawn rate. In the `Host` field, type the URL of the game API. You can also skip these steps directly from the CLI by ading the fields as arguments to the command:

```
locust -f load.py --host <https://game.api> --users <NUM_USERS> --spawn-rate <SPAWN_RATE>
```

Adding the `--autostart` flag will start the test immediately, without recquiring interaction with the web interface, while still deploying the interface as a logging visualizer.
