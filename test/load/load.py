import time
import random
from locust import HttpUser, task, between


class LocustLoadTester(HttpUser):
    wait_time = between(1, 5)
    users = []
    password = "Hello1234&"

    @task
    def register_with_credentials(self):
        userId = random.randint(0, 4000000)
        self.client.headers['Content-Type'] = 'application/json'
        response = self.client.post(
            url="/api/auth/register",
            json={
                "name": "%s" % userId,
                "email": "%s@mail.com" % userId,
                "password": self.password
            }
        )
        self.users.append(userId)

    @task
    def authenticate_with_credentials(self):
        if len(self.users) == 0:
            self.register_with_credentials()
        else:
            userId = self.users[random.randint(0, len(self.users) - 1)]
            self.client.headers['Content-Type'] = 'application/json'
            response = self.client.post(
                url="/api/auth/login",
                json={
                    "email": "%s@mail.com" % userId,
                    "password": self.password
                }
            )
