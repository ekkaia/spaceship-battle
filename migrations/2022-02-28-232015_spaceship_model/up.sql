CREATE TYPE spaceship_type AS ENUM (
    'BASIC',
    'FIGHTER'
);

CREATE TABLE "spaceship".spaceship_model (
    spaceship_model_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT UNIQUE NOT NULL, -- Nom du modèle de vaisseau
    max_health INTEGER NOT NULL CHECK (max_health >= 0), -- État initial du vaisseau
    spaceship_type spaceship_type NOT NULL, -- Type de vaisseau (indicatif)
    tank_capacity INTEGER NOT NULL CHECK (tank_capacity >= 0), -- Capacité du réservoir de carburant
    max_power INTEGER NOT NULL CHECK (max_power >= 0), -- Énergie maximum que peut délivrer le vaisseau aux modules
    consumption_for_power INTEGER NOT NULL CHECK (consumption_for_power >= 0), -- Consommation/énergie : 1 unité de carburant = X unité d'énergie
    energy_repartition_delay INTEGER NOT NULL CHECK (energy_repartition_delay >= 0), -- Délai pour changer la répartition énergétique entre les modules (en ms)
    price INTEGER NOT NULL CHECK (price >= 0) -- Coût du châssis
);