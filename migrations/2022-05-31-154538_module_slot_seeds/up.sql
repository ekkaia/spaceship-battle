INSERT INTO "spaceship".module_slot (module_slot_id, module_zone_id, module_type, size) VALUES
    ('8e348af3-f73d-47ee-a3b0-a3ff53856864', 'c79dc3bb-76e9-4d53-962e-a33d6ef78006', 'SHIELD', 3),
    ('ba10e2e4-30a0-4f3b-acf1-e8d91fe56677', 'bdf54743-5a70-4eec-987b-e207bb20eb1c', 'WEAPON', 3),
    ('c0594c15-d0e8-4ed3-9f41-3ada949a2bd3', '36a235ce-a76d-4b7a-9995-8be527c85b37', 'ENGINE', 3),
    ('31048d3d-9667-4b1e-a4b3-c961dbfa55f6', '36a235ce-a76d-4b7a-9995-8be527c85b37', 'CARGO', 3),
    ('0b64e67c-a88d-45e1-bf79-0109123ca2ac', '2d566277-36a8-48a1-908e-61c6fe223c7c', 'SHIELD', 3),
    ('f4f67f6b-74fa-4ba8-86d4-2fe40abea3f7', '6f1aae98-9db2-4c30-aa70-ef4a26fa6b0d', 'WEAPON', 3),
    ('94f782a4-8911-4c86-b725-d4753e60e31f', '36c1208d-e4a4-42d9-83ab-2a04e0303738', 'ENGINE', 3),
    ('60dcfe07-d826-4189-8569-ca23450c3196', '36c1208d-e4a4-42d9-83ab-2a04e0303738', 'CARGO', 3),
    ('0688f10d-5a44-471a-a92b-cc7886720b6f', '6464c8e3-c31d-4f58-9fa5-6302b3284413', 'SHIELD', 3),
    ('91a433a6-37b9-4bcc-be86-a94986f51ef9', 'ed2c1323-0f47-4cea-9f4f-04ff59249251', 'WEAPON', 3),
    ('93b80537-c181-4cd2-a9a7-8d3c5492093d', 'e04bbe54-8940-42e6-adb4-7f9514b480db', 'ENGINE', 3),
    ('a1e3d621-cd07-4742-8a0d-a5c30fc4a520', 'e04bbe54-8940-42e6-adb4-7f9514b480db', 'CARGO', 3);
