CREATE TABLE "spaceship".shield (
    shield_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    spaceship_id UUID REFERENCES "spaceship".spaceship (spaceship_id),
    shield_model_id UUID REFERENCES "spaceship".shield_model (shield_model_id) NOT NULL,
    member_id UUID REFERENCES "spaceship".member (member_id) NOT NULL,
    module_slot_id UUID REFERENCES "spaceship".module_slot (module_slot_id),
    health INTEGER NOT NULL CHECK (health >= 0), -- État actuel du bouclier
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TRIGGER shield_set_updated_at
    BEFORE UPDATE ON "spaceship".shield
    FOR EACH ROW
    EXECUTE PROCEDURE set_updated_at();