INSERT INTO "spaceship".engine_model (engine_model_id, name, engine_type, max_power, max_speed, max_health, size, price) VALUES
    ('f67d1d1c-cd4f-4b74-b920-fb46d5001f93', 'SOLAR Lightspeed', 'BASIC', 80, 0, 100, 1, 3900),
    ('9d11c74a-c831-4b82-83be-44d04dc07ce9', 'KALYPSO Quasar', 'BASIC', 70, 0, 90, 2, 4300),
    ('4010ebb3-a0df-4e00-9eeb-c576f95670a1', 'MIRAI Bokken', 'BASIC', 100, 0, 80 , 3, 5900);