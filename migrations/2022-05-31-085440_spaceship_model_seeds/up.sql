INSERT INTO "spaceship".spaceship_model (spaceship_model_id, name, max_health, spaceship_type, tank_capacity, max_power, consumption_for_power, energy_repartition_delay, price) VALUES
    ('c5a95a70-422f-4a17-885e-c713c86ab480', 'KALYPSO Uprise', 200, 'FIGHTER', 6000, 160, 2, 1000, 10000),
    ('ec2856f0-e758-47e8-967c-525d6f2ccac4', 'SOLAR Paradigm', 140, 'BASIC', 4000, 200, 8, 500, 7800),
    ('fe9c041f-1805-475f-9bd5-7249629b460a', 'MIRAI Streamliner', 180, 'BASIC', 3000, 130, 4, 200, 6500);