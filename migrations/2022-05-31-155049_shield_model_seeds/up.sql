INSERT INTO "spaceship".shield_model (shield_model_id, name, shield_type, shield_type_protection, max_health, reload_time, max_power, size, price) VALUES
    ('48f93d5d-7b23-4776-a083-cd90f63ea069', 'SOLAR Eclipse', 'BASIC', 20, 60, 2000, 120, 1, 1500),
    ('2ed178ee-ac4c-4837-8e59-fbe5b8d68b14', 'ARECALES Hydriss', 'BASIC', 30, 50, 2500, 140, 2, 2600),
    ('6a714edc-29c8-43e5-8410-3c035e3bdee4', 'BASILIC Crescendo', 'DISPERSION', 25, 70, 3000, 90, 3, 4000);
