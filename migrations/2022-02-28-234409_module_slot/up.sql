CREATE TYPE module_type AS ENUM (
    'CARGO',
    'SHIELD',
    'WEAPON',
    'ENGINE',
    'COMPUTER',
    'SENSOR',
    'MINING'
);

CREATE TABLE "spaceship".module_slot (
    module_slot_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    module_zone_id UUID REFERENCES "spaceship".module_zone (module_zone_id) NOT NULL,
    module_type module_type NOT NULL, -- Type de module
    size INTEGER NOT NULL -- Taille du cargo pour l'emplacement du module
);