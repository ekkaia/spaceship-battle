CREATE TYPE engine_type AS ENUM (
    'BASIC'
);

CREATE TABLE "spaceship".engine_model (
    engine_model_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT UNIQUE NOT NULL, -- Nom du moteur
    engine_type engine_type NOT NULL, -- Type de moteur
    max_power INTEGER NOT NULL CHECK (max_power >= 0), -- Énergie demandée pour être au max des capacités du moteur
    max_speed INTEGER NOT NULL CHECK (max_speed >= 0), -- Vitesse maximale en m/s
    max_health INTEGER NOT NULL CHECK (max_health >= 0), -- État initial de l'arme
    size INTEGER NOT NULL CHECK (size >= 0), -- Taille du moteur pour l'emplacement du module
    price INTEGER NOT NULL -- Coût du module
);