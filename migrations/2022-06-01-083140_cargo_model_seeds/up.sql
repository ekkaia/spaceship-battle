INSERT INTO "spaceship".cargo_model (cargo_model_id, name, cargo_type, max_health, capacity, size, price) VALUES
    ('b31e18dd-dd4f-486b-b479-5cfc02509123', 'BASILIC Carrier', 'BASIC', 50, 100, 1, 1200),
    ('8769b444-c816-49e9-b603-fcf5a4be9b1e', 'MIRAI Bakku pakku', 'BASIC', 40, 200, 2, 2400),
    ('05fdcd59-3619-41f6-89c0-d4eb30a7a57c', 'KALYPSO Barracuda', 'RADIATION_PROTECTION', 70, 150, 3, 3800);