INSERT INTO "spaceship".module_zone (module_zone_id, spaceship_model_id, shielding) VALUES
    ('c79dc3bb-76e9-4d53-962e-a33d6ef78006', 'c5a95a70-422f-4a17-885e-c713c86ab480', 100),
    ('bdf54743-5a70-4eec-987b-e207bb20eb1c', 'c5a95a70-422f-4a17-885e-c713c86ab480', 50),
    ('36c1208d-e4a4-42d9-83ab-2a04e0303738', 'c5a95a70-422f-4a17-885e-c713c86ab480', 50),
    ('2d566277-36a8-48a1-908e-61c6fe223c7c', 'fe9c041f-1805-475f-9bd5-7249629b460a', 40),
    ('6f1aae98-9db2-4c30-aa70-ef4a26fa6b0d', 'fe9c041f-1805-475f-9bd5-7249629b460a', 60),
    ('36a235ce-a76d-4b7a-9995-8be527c85b37', 'fe9c041f-1805-475f-9bd5-7249629b460a', 40),
    ('6464c8e3-c31d-4f58-9fa5-6302b3284413', 'ec2856f0-e758-47e8-967c-525d6f2ccac4', 60),
    ('ed2c1323-0f47-4cea-9f4f-04ff59249251', 'ec2856f0-e758-47e8-967c-525d6f2ccac4', 60),
    ('e04bbe54-8940-42e6-adb4-7f9514b480db', 'ec2856f0-e758-47e8-967c-525d6f2ccac4', 60);
