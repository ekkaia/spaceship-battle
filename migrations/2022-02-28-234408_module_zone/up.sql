CREATE TABLE "spaceship".module_zone (
    module_zone_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    spaceship_model_id UUID REFERENCES "spaceship".spaceship_model (spaceship_model_id) NOT NULL,
    shielding INTEGER NOT NULL-- Blindage du châssis, absorbe des dégâts. Ce qui n'est pas absorbé est infligé aux modules
);
