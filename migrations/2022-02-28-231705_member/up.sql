CREATE TABLE "spaceship".member (
    member_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT UNIQUE NOT NULL CHECK (length(name) >= 3 AND length(name) <= 16), -- Pseudo
    email TEXT UNIQUE NOT NULL,
    credits INTEGER NOT NULL DEFAULT 0, -- Amount of money
    password TEXT NOT NULL,
    is_admin BOOLEAN NOT NULL DEFAULT false, -- Is the user an admin?
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TRIGGER member_set_updated_at
    BEFORE UPDATE ON "spaceship".member
    FOR EACH ROW
    EXECUTE PROCEDURE set_updated_at();