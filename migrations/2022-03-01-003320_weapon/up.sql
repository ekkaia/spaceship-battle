CREATE TABLE "spaceship".weapon (
    weapon_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    spaceship_id UUID REFERENCES "spaceship".spaceship (spaceship_id),
    weapon_model_id UUID REFERENCES "spaceship".weapon_model (weapon_model_id) NOT NULL,
    module_slot_id UUID REFERENCES "spaceship".module_slot (module_slot_id),
    member_id UUID REFERENCES "spaceship".member (member_id) NOT NULL,
    health INTEGER NOT NULL CHECK (health >= 0), -- État de l'arme actuel
    ammo INTEGER NOT NULL CHECK (ammo >= 0), -- Munitions actuellement chargées dans l'arme
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TRIGGER weapon_set_updated_at
    BEFORE UPDATE ON "spaceship".weapon
    FOR EACH ROW
    EXECUTE PROCEDURE set_updated_at();