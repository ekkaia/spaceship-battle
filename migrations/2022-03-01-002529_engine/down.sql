DROP TABLE IF EXISTS "spaceship".engine CASCADE;
DROP TRIGGER IF EXISTS engine_set_updated_at ON "spaceship".engine CASCADE;