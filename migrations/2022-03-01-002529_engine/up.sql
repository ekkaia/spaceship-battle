CREATE TABLE "spaceship".engine (
    engine_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    spaceship_id UUID REFERENCES "spaceship".spaceship (spaceship_id),
    engine_model_id UUID REFERENCES "spaceship".engine_model (engine_model_id) NOT NULL,
    member_id UUID REFERENCES "spaceship".member (member_id) NOT NULL,
    module_slot_id UUID REFERENCES "spaceship".module_slot (module_slot_id),
    health INTEGER NOT NULL CHECK (health >= 0), -- État initial du moteur
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TRIGGER engine_set_updated_at
    BEFORE UPDATE ON "spaceship".engine
    FOR EACH ROW
    EXECUTE PROCEDURE set_updated_at();